CREATE TABLE "process_definition" (
	"id" varchar(255) NOT NULL,
	"key" varchar(255) NOT NULL,
	"name" varchar(255) NOT NULL,
	"deployment_id" varchar(255) NOT NULL UNIQUE,
	"version" integer NOT NULL,
	"schema_xml" xml,
	CONSTRAINT "ProcessDefinition_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

create type process_instance_state as enum ('ACTIVE', 'SUSPENDED', 'COMPLETED', 'EXTERNALLY_TERMINATED', 'INTERNALLY_TERMINATED');

CREATE TABLE "process_instance" (
	"id" varchar(255) NOT NULL,
	"definition_id" varchar(255) NOT NULL,
	"start_user_id" varchar(255),
	"start_time" TIMESTAMP NOT NULL,
	"end_time" TIMESTAMP,
	"duration_in_millis" bigint,
	"removal_time" TIMESTAMP,
-- 	"start_activity_id" varchar(255) NOT NULL,
	"state" process_instance_state NOT NULL,
	"root_process_instance_id" varchar(255),
	"last_sync_time" TIMESTAMP NOT NULL,
	CONSTRAINT "ProcessInstance_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "task" (
	"id" varchar(255) NOT NULL,
	"parent_task_id" varchar(255),
	"name" varchar(255) NOT NULL,
	"task_definition_key" varchar(255) NOT NULL,
	"description" varchar(255),
	"start_time" TIMESTAMP NOT NULL,
	"end_time" TIMESTAMP,
	"owner" varchar(255),
	"assignee" varchar(255),
	"removal_time" TIMESTAMP,
	"due_time" TIMESTAMP,
	"followup_time" TIMESTAMP,
	"activity_instance_id" varchar(255),
	"priority" integer NOT NULL,
	CONSTRAINT "Task_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

create type activity_instance_type as enum (
    'cancelBoundaryCatch',
    'compensationBoundaryCatch',
    'boundaryConditional',
    'boundaryError',
    'boundaryEscalation',
    'boundaryMessage',
    'boundarySignal',
    'boundaryTimer',
    'callActivity',
    'cancelEndEvent',
    'compensationEndEvent',
    'errorEndEvent',
'escalationEndEvent',
'messageEndEvent',
'noneEndEvent',
'signalEndEvent',
'terminateEndEvent',
'complexGateway',
'eventBasedGateway',
'exclusiveGateway',
'inclusiveGateway',
'parallelGateway',
'intermediateCatchEvent',
'intermediateCompensationThrowEvent',
'intermediateConditional',
'intermediateEscalationThrowEvent',
'intermediateLinkCatch',
'intermediateMessageCatch',
'intermediateMessageThrowEvent',
'intermediateNoneThrowEvent',
'intermediateSignalCatch',
'intermediateSignalThrow',
'intermediateThrowEvent',
'intermediateTimer',
'multiInstanceBody',
'startEvent',
'compensationStartEvent',
'conditionalStartEvent',
'errorStartEvent',
'escalationStartEvent',
'messageStartEvent',
'signalStartEvent',
'startTimerEvent',
'subProcess',
'adHocSubProcess',
'task',
'businessRuleTask',
'manualTask',
'receiveTask',
'scriptTask',
'sendTask',
'serviceTask',
'userTask',
'transaction'
);

CREATE TABLE "activity_instance" (
	"id" varchar(255) NOT NULL unique,
	"parent_activity_instance_id" varchar(255),
	"type" activity_instance_type,
    "name" varchar(255),
	"activity_id" varchar(255) NOT NULL,
	"process_instance_id" varchar(255) NOT NULL,
	"start_time" TIMESTAMP NOT NULL,
	"end_time" TIMESTAMP,
	"duration_in_millis" bigint,
	"removal_time" TIMESTAMP,
	"canceled" BOOLEAN NOT NULL,
	"execution_id" varchar(255) NOT NULL,
	CONSTRAINT "ActivityInstance_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "execution" (
	"id" varchar(255) NOT NULL,
	"process_instance_id" varchar(255) NOT NULL,
	"ended" BOOLEAN NOT NULL,
	CONSTRAINT "Execution_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "user" (
	"id" varchar(255) NOT NULL,
	"first_name" varchar(255) NOT NULL,
	"last_name" varchar(255) NOT NULL,
	"email" varchar(255) NOT NULL UNIQUE,
	CONSTRAINT "User_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

create type variable_type as enum ('Boolean', 'Bytes', 'Short', 'Integer', 'Long'
    ,'Double', 'Date', 'String', 'Null', 'File', 'Object', 'Json', 'Xml');

create type variable_state as enum ('CREATED', 'DELETED');

CREATE TABLE "variable_instance" (
    "id" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "process_instance_id" varchar(255) NOT NULL,
    "execution_id" varchar(255) NOT NULL,
    "state" variable_state NOT NULL,
    "create_time" TIMESTAMP NOT NULL,
    CONSTRAINT "Variable_instance_pk" PRIMARY KEY ("id")
) WITH (
      OIDS=FALSE
    );

CREATE TABLE "variable_history"
(
    "id"                  varchar(255)   NOT NULL,
    "variable_instance_id" varchar(255)   NOT NULL,
    "task_id" varchar(255),
    "time"         TIMESTAMP     NOT NULL,
    "time_zone"    varchar(127),
    "scope"                varchar(255)   NOT NULL,
    "type" jsonb NOT NULL,
    "value" jsonb,
    "value_info" varchar(4096),
    CONSTRAINT "Variable_history_pk" PRIMARY KEY ("id")
) WITH (
      OIDS= FALSE
    );

CREATE TABLE "incident" (
	"id" varchar(255) NOT NULL,
	"activity_id" varchar(255) NOT NULL,
	"create_time" TIMESTAMP NOT NULL,
	"process_instance_id" varchar(255) NOT NULL,
	"type" varchar(255) NOT NULL,
	"message" varchar(255),
	"open" BOOLEAN NOT NULL,
	"deleted" BOOLEAN NOT NULL,
	"resolved" BOOLEAN NOT NULL,
    CONSTRAINT "Incident_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "properties" (
  "property" varchar(255),
  "value" varchar(255)
) WITH (
	OIDS=FALSE
);

ALTER TABLE "process_instance" ADD CONSTRAINT "ProcessInstance_fk0" FOREIGN KEY ("definition_id") REFERENCES "process_definition"("id");
ALTER TABLE "process_instance" ADD CONSTRAINT "ProcessInstance_fk1" FOREIGN KEY ("start_user_id") REFERENCES "user"("id");
-- ALTER TABLE "process_instance" ADD CONSTRAINT "ProcessInstance_fk2" FOREIGN KEY ("start_activity_id") REFERENCES "activity_instance"("id");
ALTER TABLE "process_instance" ADD CONSTRAINT "ProcessInstance_fk4" FOREIGN KEY ("root_process_instance_id") REFERENCES "process_instance"("id");

ALTER TABLE "task" ADD CONSTRAINT "Task_fk0" FOREIGN KEY ("parent_task_id") REFERENCES "task"("id");
ALTER TABLE "task" ADD CONSTRAINT "Task_fk1" FOREIGN KEY ("owner") REFERENCES "user"("id");
ALTER TABLE "task" ADD CONSTRAINT "Task_fk2" FOREIGN KEY ("assignee") REFERENCES "user"("id");
ALTER TABLE "task" ADD CONSTRAINT "Task_fk3" FOREIGN KEY ("activity_instance_id") REFERENCES "activity_instance"("id");

ALTER TABLE "activity_instance" ADD CONSTRAINT "ActivityInstance_fk0" FOREIGN KEY ("parent_activity_instance_id") REFERENCES "activity_instance"("id");
ALTER TABLE "activity_instance" ADD CONSTRAINT "ActivityInstance_fk1" FOREIGN KEY ("process_instance_id") REFERENCES "process_instance"("id");
-- ALTER TABLE "activity_instance" ADD CONSTRAINT "ActivityInstance_fk2" FOREIGN KEY ("execution_id") REFERENCES "execution"("id");

ALTER TABLE "execution" ADD CONSTRAINT "Execution_fk0" FOREIGN KEY ("process_instance_id") REFERENCES "process_instance"("id");
-- ALTER TABLE "variable" ADD CONSTRAINT "Variable_fk1" FOREIGN KEY ("process_instance_id") REFERENCES "process_instance"("id");
ALTER TABLE "variable_instance" ADD CONSTRAINT "Variable_fk1" FOREIGN KEY ("process_instance_id") REFERENCES "process_instance"("id");
-- ALTER TABLE "variable_instance" ADD CONSTRAINT "Variable_fk2" FOREIGN KEY ("execution_id") REFERENCES "execution"("id");
ALTER TABLE "variable_history" ADD CONSTRAINT "Variable_fk3" FOREIGN KEY ("variable_instance_id") REFERENCES "variable_instance"("id");
ALTER TABLE "variable_history" ADD CONSTRAINT "Variable_fk4" FOREIGN KEY ("task_id") REFERENCES "task"("id");
-- ALTER TABLE "incident" ADD CONSTRAINT "Incident_fk0" FOREIGN KEY ("activity_id") REFERENCES "activity_instance"("activity_id");
ALTER TABLE "incident" ADD CONSTRAINT "Incident_fk1" FOREIGN KEY ("process_instance_id") REFERENCES "process_instance"("id");

