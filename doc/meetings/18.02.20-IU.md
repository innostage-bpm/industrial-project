# Meeting 18 Feb in university


**Place, time**: IU, 9:40

**Participants**: Ильдар, Владимир, Евгений, Сергей

**Agenda**:  

 Обсуждение Quality plan milestone 1, отчёт за неделю

**Summary**:

Получили рекомендации по изменению Quality plan, а именно: добавить Glossary, Scope of Work, Actors Persona, add new description of quality scenarios in terms of [source, stimulus, environment, artifact, response, response measure].

Jira предоставлена в ближайшее время не будет, работаем в своем репозитории, тулза для трекинга задач на наш выбор.
