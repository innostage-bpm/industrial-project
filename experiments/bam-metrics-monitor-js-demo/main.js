let api = 'http://localhost:8080/engine-rest';
document.getElementById('apiurl').value = api;


document.getElementById('runningInstances')
    .addEventListener('click', () => renderRequest('/process-instance'));

document.getElementById('openIncidents')
    .addEventListener('click', () => renderRequest('/incident'));

document.getElementById('openHumanTask')
    .addEventListener('click', () => renderRequest('/task'));

document.getElementById('processDefinitions')
    .addEventListener('click', () => renderRequest('/process-definition?latestVersion=true'));

document.getElementById('decisionDefinitions')
    .addEventListener('click', () => renderRequest('/decision-definition?latestVersion=true'));

document.getElementById('caseDefinitions')
    .addEventListener('click', () => renderRequest('/case-definition?latestVersion=true'));

document.getElementById('deployments')
    .addEventListener('click', () => renderRequest('/deployment'));

async function renderRequest(request) {
    api = document.getElementById('apiurl').value;
    try {
        const response = await axios.get(`${api}${request}`);
        renderTable(response.data);
    } catch (error) {
        console.error(error);
    }
}

function renderTable(data) {
    let parentEl = document.querySelector(`.renderData`);
    parentEl.innerHTML = '';

    let table = document.createElement('table');
    table.classList.add('table' , 'table-hover', 'table-bordered', 'table-sm');

    let thead = document.createElement('thead');

    let tr = document.createElement('tr');
    tr.classList.add('table-primary');

    for (let header of Object.keys(data[0])) {
        let th = document.createElement('th');
        th.innerHTML = header;
        tr.append(th);
    }

    thead.append(tr);
    table.append(thead);

    let tbody = document.createElement('tbody');

    for (let row of data) {
        let tr = document.createElement('tr');

        for (let data of Object.values(row)) {
            if (Array.isArray(data) && data.length === 0) data = null;
            let td = document.createElement('td');
            td.innerHTML = data || 'null';
            tr.append(td);
        }

        tbody.append(tr);
    }

    table.append(tbody);
    parentEl.append(table);
}