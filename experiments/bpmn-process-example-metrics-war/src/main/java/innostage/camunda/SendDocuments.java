package innostage.camunda;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.history.HistoricActivityInstance;

import java.util.List;
import java.util.logging.Logger;

public class SendDocuments implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(SendDocuments.class.getName());
    ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        long cost = (long) delegateExecution.getVariable("cost");
        LOGGER.info("cost send " + cost);
    }



}
