package innostage.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class ReceiveDocuments implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(ReceiveDocuments.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        long cost = (long) delegateExecution.getVariable("cost");
        LOGGER.info("cost receive " + cost);
    }


}
