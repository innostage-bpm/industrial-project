package innostage.camunda;

import innostage.camunda.services.MetricsWriter;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.management.Metrics;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.logging.Logger;

public class MetricsCollector implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(innostage.camunda.SendDocuments.class.getName());
    private ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    private static final String PROCESS_DEFINITION_KEY = "Property_Registration";

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LoggerDelegate loggerDelegate = new LoggerDelegate();
        loggerDelegate.execute(delegateExecution);
        showHistory();
    }

    public void showHistory() {

        String processInstanceId = processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .processDefinitionKey(PROCESS_DEFINITION_KEY)
                .list().get(getNumberOfLastExecutionProcess())
                .getId();

        List<HistoricActivityInstance> activityInstances = processEngine.getHistoryService().createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId)
                .orderPartiallyByOccurrence().asc().list();

//        listActivities(activityInstances);
        logActivities(activityInstances);
    }

    private int getNumberOfLastExecutionProcess() {
        return processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .processDefinitionKey("Property_Registration").list().size() - 1;
    }

    private void logActivities(List<HistoricActivityInstance> activityInstances) {
        MetricsWriter metricsWriter = new MetricsWriter();
        try {
            metricsWriter.clearMetricsTable();
            metricsWriter.writeLogs(activityInstances);
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    private void listActivities(List<HistoricActivityInstance> activityInstances) {
        for (HistoricActivityInstance activityInstance : activityInstances) {
            LOGGER.info("\n ACTIVITY NAME: " + activityInstance.getActivityName());
            LOGGER.info( "\n ACTIVITY TYPE: " +  (activityInstance.getActivityType()));
            LOGGER.info("\n Activity assigned to: " + activityInstance.getAssignee());
            LOGGER.info("\n Process definition id: " + activityInstance.getProcessDefinitionId());
            LOGGER.info("\n Process instance id: " + activityInstance.getProcessInstanceId());
            LOGGER.info( "\n Start time: " +  (activityInstance.getStartTime()));
        }
    }
}
