package innostage.camunda.services;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import org.camunda.bpm.engine.history.HistoricActivityInstance;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class MetricsWriter {
    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private final static Logger LOGGER = Logger.getLogger(innostage.camunda.services.MetricsWriter.class.getName());
    private final static String SPREADSHEET_ID = "1rZlx7DtwaZcm-cE3tD1xKp93I38z1DmR1GRdWK4aY24";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = MetricsWriter.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public void writeLogs (List<HistoricActivityInstance> activityInstances) throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

        List<ValueRange> data = new ArrayList<>();
        data.add(new ValueRange()
                .setRange("A1:I1")
                .setValues(Arrays.asList(
                        Arrays.asList("Instance id", "Process definition id", "Order by occurrence",
                                "Activity type", "Activity name", "Start time", "Execution time", "Assigned to"))));

        int i = 0;
        for (HistoricActivityInstance activityInstance : activityInstances) {
            i++;
            data.add(new ValueRange()
                    .setRange("A" + (i + 1))
                    .setValues(Arrays.asList(
                            Arrays.asList(activityInstance.getProcessInstanceId() + " ", activityInstance.getProcessDefinitionId() + " ",
                                    i, activityInstance.getActivityType()+ " ",
                                    activityInstance.getActivityName()+ " ", activityInstance.getStartTime().toString()+ " ",
//                                    activityInstance.getEndTime().toString(),
                                    activityInstance.getDurationInMillis()+ " ",
                                    activityInstance.getAssignee() + " "))));
        }

        BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
                .setValueInputOption("USER_ENTERED")
                .setData(data);


        BatchUpdateValuesResponse batchResult = service.spreadsheets().values()
                .batchUpdate(SPREADSHEET_ID, batchBody)
                .execute();
    }

    //For tests
    public static void main (String[] args) throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String range = "Metrics";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

//        List<ValueRange> data = new ArrayList<>();
//        data.add(new ValueRange()
//                .setRange("A1:H1")
//                .setValues(Arrays.asList(
//                        Arrays.asList("Process id ", "Order by occurance", "Activity type", "Activity name", "Start time", "End time", "Execution time", "Assigned to"))));
//        data.add(new ValueRange()
//                .setRange("A2")
//                .setValues(Arrays.asList(
//                        Arrays.asList("February Total", "fvdfv", "fvdfv", "fvdfv", "fvdfv", "fvdfv", "fvdfv", "fvdfv"))));
//
//        BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
//                .setValueInputOption("USER_ENTERED")
//                .setData(data);

        ClearValuesRequest requestBody = new ClearValuesRequest();

        Sheets.Spreadsheets.Values.Clear request =
                service.spreadsheets().values().clear(SPREADSHEET_ID, range, requestBody);

        ClearValuesResponse response = request.execute();
    }

    public void clearMetricsTable() throws GeneralSecurityException, IOException {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String range = "Metrics";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        ClearValuesRequest requestBody = new ClearValuesRequest();
        Sheets.Spreadsheets.Values.Clear request =
                service.spreadsheets().values().clear(SPREADSHEET_ID, range, requestBody);

        ClearValuesResponse response = request.execute();
    }
}