package innostage.camunda.services;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


public class RequestApiMaker {

    private static final String APIKEY = "8a1c6703023c2366117dbba551cf9ed7";

    // one instance, reuse
    private final OkHttpClient httpClient = new OkHttpClient();

    public String sendGet(String city) throws Exception {
        String resp = "";
        Request request = new Request.Builder()
                .url("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + APIKEY)
                .addHeader("Content-type", "application/json")
                .build();

        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            resp = response.body().string();
        }

        return resp;
    }

    //for tests
    public static void main(String[] args) throws Exception {
        System.out.println(new RequestApiMaker().sendGet("Moscow,ru"));
    }

}
