package innostage.camunda.services;

import okhttp3.*;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.RuntimeService;

import java.io.IOException;

public class IncidentSimulator {

    // one instance, reuse
    private final OkHttpClient httpClient = new OkHttpClient();
    private ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    private RuntimeService runtimeService = processEngine.getRuntimeService();
    private static final String PROCESS_DEFINITION_KEY = "Property_Registration";

    private void sendPost() throws Exception {

        // form parameters
        RequestBody formBody = new FormBody.Builder()
                .add("incidentType", "aSimulatedIncident")
                .add("configuration", "aConfiguration")
                .build();

        Request request = new Request.Builder()
                .url("http://localhost:8080/engine-rest/execution/03aea429-818d-11ea-bc8a-164f8a355b88/create-incident")
                .addHeader("content-type", "application/json")
                .post(formBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            // Get response body
            System.out.println(response.body().string());
        }

    }
    public static void main(String[] args) throws Exception {

        IncidentSimulator incidentSimulator = new IncidentSimulator();
//        incidentSimulator.sendPost();
        incidentSimulator.createIncidentByJava();
    }

    private void createIncidentByJava() {
        runtimeService.createIncident("someType", "03aea429-818d-11ea-bc8a-164f8a355b88", "someConfiguration", "someMessage");
    }

    private int getNumberOfLastExecutionProcess() {
        return processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .processDefinitionKey("Property_Registration").list().size() - 1;
    }

    private String getProcessInstanceId() {
        String processInstanceId = processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .processDefinitionKey(PROCESS_DEFINITION_KEY)
                .list().get(getNumberOfLastExecutionProcess())
                .getId();
        return processInstanceId;
    }


}
