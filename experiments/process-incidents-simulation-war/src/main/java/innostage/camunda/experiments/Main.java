package innostage.camunda.experiments;

import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.management.Metrics;

public class Main {


    public static void main(String[] args) {

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        ManagementService managementService = processEngine.getManagementService();

        long numCompletedActivityInstances = managementService
                .createMetricsQuery()
                .name(Metrics.ACTIVTY_INSTANCE_START)
                .sum();

        System.out.println(numCompletedActivityInstances);
    }

}
