package innostage.camunda;

import innostage.camunda.services.RequestApiMaker;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class ReceiveDocuments implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(ReceiveDocuments.class.getName());
    private ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    private RuntimeService runtimeService = processEngine.getRuntimeService();
    private static final String PROCESS_DEFINITION_KEY = "Property_Registration";

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        long cost = (long) delegateExecution.getVariable("cost");
        LOGGER.info("cost receive " + cost);
        LOGGER.info("Current process instance id:" + getProcessInstanceId());

        simulateAnIncident();
//        RequestApiMaker requestApiMaker = new RequestApiMaker();
//        try {
//            String response = requestApiMaker.sendGet("Moscccow,ru");
//        } catch (Exception ex) {
//            throw new BpmnError("External API exception");
//        }
    }

    private void simulateAnIncident() {
        runtimeService.createIncident("someType", getProcessInstanceId(), "someConfiguration", "someMessage");
    }

    private int getNumberOfLastExecutionProcess() {
        return processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .processDefinitionKey(PROCESS_DEFINITION_KEY).list().size() - 1;
    }

    private String getProcessInstanceId() {
        String processInstanceId = processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .processDefinitionKey(PROCESS_DEFINITION_KEY)
                .list().get(getNumberOfLastExecutionProcess())
                .getId();
        return processInstanceId;
    }





}
