package innostage.camunda;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.HistoricActivityInstanceQueryImpl;

import java.util.logging.Logger;

public class NotificationSender implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(NotificationSender.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.info("notification was send");
    }

}
