package innopolis.innostage.metric_collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetricCollectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetricCollectionApplication.class, args);
	}

}
