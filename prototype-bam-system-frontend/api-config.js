//export const apiUrl = 'https://bam-back-end-api.herokuapp.com';

class ApiConfig {
    constructor() {
        this.apiUrl = 'https://bam-backend.herokuapp.com'
    }

    setApi(url) {
        this.apiUrl = url;
    }
}

const apiConfig = new ApiConfig();

export default apiConfig;