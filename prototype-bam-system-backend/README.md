# Backend for Business Process Monitoring

This module represents an adapter for Camunda BPM engine. 

The main goal of this application is to save all the information about history of runnning process in Postgres database via Hibernate entities.

The [entities are](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/prototype-bam-system-backend/src/main/java/innostage/camunda/models): process definition, process instances, activities, incidents, variables, tasks. The detailed [data model is here](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/architecture/ER-diagram.pdf).

The data from engine [should be synchronized](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/prototype-bam-system-backend/src/main/java/innostage/camunda/services/MetricAggregator.java) with the Postgres database by predefined schedule.

The instance of Camunda Engine should be running on the machine by address, that is configured in [this file](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/prototype-bam-system-backend/src/main/resources/engine_properties.json).
The .bpmn process should be deployed in the system and started for execution.

The rough sequence of actions for the simple use-case is the following:

1. [Download the package](https://downloads.camunda.cloud/release/camunda-bpm/run/) with Camunda engine
2. [Start the tomcat server with Camunda engine](https://docs.camunda.org/manual/7.13/installation/camunda-bpm-run/) by address localhost:8081/camunda
3. Deploy any [.bpmn diagram file](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/doc/bpmn%20diagrams) via [REST API request](https://docs.camunda.org/manual/7.7/reference/rest/deployment/)
4. Start the deployed process definition by [REST API request](https://docs.camunda.org/manual/7.9/reference/rest/process-definition/post-start-process-instance/)
5. Set up the [Postgres database](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/architecture/camunda_postgres_create.sql)
5. Start the current Spring Boot application of BAM-system by address localhost:8080
6. The data about all entities should be automatically (and correctly!) saved into Postgres database
7. The data can be vizualized by user in [prototype-bam-system-frontend module](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/prototype-bam-system-frontend)