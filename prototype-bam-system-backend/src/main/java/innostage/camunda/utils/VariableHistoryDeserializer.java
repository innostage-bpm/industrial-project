package innostage.camunda.utils;

import com.google.gson.*;
import innostage.camunda.models.VariableHistoryEntity;
import innostage.camunda.models.enums.VariableType;
import org.postgresql.util.PGobject;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VariableHistoryDeserializer implements JsonDeserializer<VariableHistoryEntity>  {

    private static final String CAMUNDA_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final String DB_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final int TIME_ZONE_POSTFIX_LENGTH = 5;

    @Override
    public VariableHistoryEntity deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject object = jsonElement.getAsJsonObject();
        if(!object.get("type").getAsString().equals("variableUpdate"))
            return null;
        String varType = object.get("variableType").getAsString();
        boolean isCaseDefinitions = !object.get("caseDefinitionId").isJsonNull();
        if (isCaseDefinitions) {
            return null;
        }
        VariableHistoryEntity variable = new VariableHistoryEntity();
        variable.setId(object.get("id").getAsString());
        variable.setVariableInstanceId(object.get("variableInstanceId").getAsString());
        variable.setType(VariableType.valueOf(varType));

        if (object.get("value").isJsonNull()) {
            variable.setValue(null);
        } else {
            PGobject jsonObject = get_json_object(object, "value");
            variable.setValue(jsonObject);
        }

        PGobject jsonObject = get_json_object(object, "valueInfo");
        variable.setValueInfo(jsonObject);

        if (!object.get("taskId").isJsonNull())
            variable.setTaskId(object.get("taskId").getAsString());
        String time = object.get("time").getAsString();
        variable.setTime(getFormattedTime(time));
        variable.setTimeZone(getTimeZone(time));
        variable.setScope(object.get("activityInstanceId").getAsString());
        return variable;
    }

    private PGobject get_json_object(JsonObject object, String value) {
        PGobject jsonObject = new PGobject();
        jsonObject.setType("json");
        try {
            jsonObject.setValue(object.get(value).toString());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return jsonObject;
    }

    private Timestamp getFormattedTime(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat(CAMUNDA_DATETIME_FORMAT);
        SimpleDateFormat output = new SimpleDateFormat(DB_DATETIME_FORMAT);
        Date d = null;
        try {
            d = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Timestamp.valueOf(output.format(d));
    }

    private String getTimeZone(String time) {
        return time.substring(time.length() - TIME_ZONE_POSTFIX_LENGTH);
    }
}
