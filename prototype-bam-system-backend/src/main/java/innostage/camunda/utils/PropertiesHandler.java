package innostage.camunda.utils;

import innostage.camunda.BpmnAdaptorApplication;
import innostage.camunda.services.exceptions.EngineAddressIsInvalidException;
import innostage.camunda.services.exceptions.SyncPeriodTimeIsInvalidException;
import org.apache.commons.lang.SystemUtils;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

@Component
public class PropertiesHandler {

    public static final String LOCAL_ENGINE_ADDRESS = "localhost:8081/engine-rest";
    public static final String DEFAULT_SYNC_PERIOD = "20";

    private final String QUERY_CREATE_PROPERTY_TABLE = "CREATE TABLE properties (\"property\" varchar(255), \"value\" varchar(255)) WITH (OIDS=FALSE);";
    private final String QUERY_INSERT_DEFAULT_ENGINE_ADDRESS ="INSERT INTO properties (property, value) VALUES ('engine_address', ?)";
    private final String QUERY_INSERT_DEFAULT_SYNCH_PERIOD ="INSERT INTO properties (property, value) VALUES ('synch_period_sec', ?)";
    private final String QUERY_UPDATE_PROPERTY = "UPDATE properties SET value = ? WHERE property = ?";
    private final String QUERY_SELECT_PROPERTY = "SELECT value FROM properties WHERE property = ?";
    private final String PROTOCOL_REGEXP = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?";
    private final String ADDRESS_REGEXP = "([a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}|localhost)(:[0-9]{1,5})?(\\/)?[a-zA-Z0-9]+([\\-\\.]{1}[a-z0-9]+)*$";
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private final Properties prop;
    private Connection con;


    public Long getSyncTimePeriod(){
        return Long.valueOf(getPropertyFromDatabase("synch_period_sec"));
    }

    public PropertiesHandler() {
        this.prop = new Properties();
        try (InputStream inputStream = BpmnAdaptorApplication.class.getResourceAsStream("/application.properties")) {
            prop.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace(System.out);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        String url = getProperty("spring.datasource.url");
        String user = getProperty("spring.datasource.username");
        String password = getProperty("spring.datasource.password");
        try { con = DriverManager.getConnection(url, user, password);} catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getEndpoint(String endpointName) {
        return prop.getProperty(endpointName);
    }

    public String getProperty(String propertyName) {
        return System.getProperty(propertyName) == null ? prop.getProperty(propertyName) : System.getProperty(propertyName);
    }

    public String getBpmnEngineAddress() {
        return getPropertyFromDatabase("engine_address");
    }

    public void setBpmnEngineAddress(String engineAddress) throws EngineAddressIsInvalidException {
        if(engineAddress.matches(PROTOCOL_REGEXP+ADDRESS_REGEXP)) {
            engineAddress = engineAddress.replaceAll(PROTOCOL_REGEXP,"");
            setPropertyInDatabase("engine_address",engineAddress);
        } else {
            throw new EngineAddressIsInvalidException(engineAddress);
        }
    }

    public void setSyncPeriodTime(String syncPeriodTime) throws SyncPeriodTimeIsInvalidException{
        if(syncPeriodTime.matches("\\d+")) {
            setPropertyInDatabase("synch_period_sec", syncPeriodTime);
        } else {
            throw new SyncPeriodTimeIsInvalidException(syncPeriodTime);
        }
    }

    public void createPropertiesTableIfNotExist() {
        try {
            ResultSet tables = con.getMetaData().getTables(null, null, "properties", null);
            if (!tables.next()) {

                try (Statement st = con.createStatement()) {
                    st.executeUpdate(QUERY_CREATE_PROPERTY_TABLE);
                } catch (SQLException ex) {
                    LOGGER.severe("Error in creating default table with application properties");
                }

                try (PreparedStatement pst = con.prepareStatement(QUERY_INSERT_DEFAULT_ENGINE_ADDRESS)) {
                    pst.setString(1, this.getProperty("engine.address") != null ? this.getProperty("engine.address") : LOCAL_ENGINE_ADDRESS );
                    pst.executeUpdate();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                try (PreparedStatement pst = con.prepareStatement(QUERY_INSERT_DEFAULT_SYNCH_PERIOD)) {
                    pst.setString(1, this.getProperty("sync.period") != null ? this.getProperty("sync.period") : DEFAULT_SYNC_PERIOD );
                    pst.executeUpdate();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getPropertyFromDatabase(String propertyName) {
        createPropertiesTableIfNotExist();
        try (PreparedStatement pst = con.prepareStatement(QUERY_SELECT_PROPERTY))
        {
            pst.setString(1, propertyName);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return(rs.getString(1));
            } else {
                LOGGER.severe("There is no application property '" + propertyName + "' in database");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void setPropertyInDatabase(String propertyName, String value) {
        createPropertiesTableIfNotExist();
        try (PreparedStatement pst = con.prepareStatement(QUERY_UPDATE_PROPERTY))
        {
            pst.setString(1, value);
            pst.setString(2, propertyName);
            pst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        LOGGER.info("The application property " + propertyName + " was set to " + value);
    }

    public String getEngineHost() {
	    String host = getBpmnEngineAddress().replaceAll(PROTOCOL_REGEXP,"");
	    if (host.contains(":")) {
	        return host.split(":")[0];
        } else if (host.contains("/")) {
            return host.split("/")[0];
        }
	    return host;
    }

    public int getEnginePort() {
        URL url = null;
        try {
            url = new URL("http://" + getBpmnEngineAddress());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url != null ? url.getPort() : -1;
    }

    public static String getDriverForThisOS() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return "target/classes/chromedriver.exe";
        } else if (SystemUtils.IS_OS_LINUX) {
            return "target/classes/chromedriver";
        }
        return null;
    }
}
