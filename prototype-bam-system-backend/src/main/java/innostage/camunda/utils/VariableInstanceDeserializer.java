package innostage.camunda.utils;

import com.google.gson.*;
import innostage.camunda.models.VariableInstanceEntity;

import java.lang.reflect.Type;

public class VariableInstanceDeserializer implements JsonDeserializer<VariableInstanceEntity>  {
    @Override
    public VariableInstanceEntity deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject object = jsonElement.getAsJsonObject();
        boolean isCaseDefinitions = !object.get("caseDefinitionId").isJsonNull();
        if (isCaseDefinitions) {
            return null;
        }
        VariableInstanceEntity variable = new Gson().fromJson(object,VariableInstanceEntity.class);
        return variable;
    }
}
