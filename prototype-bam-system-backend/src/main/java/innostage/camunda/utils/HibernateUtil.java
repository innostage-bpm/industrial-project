package innostage.camunda.utils;


import innostage.camunda.models.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static final PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                // Hibernate settings equivalent to hibernate.cfg.xml's properties
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, PROPERTIES_HANDLER.getProperty("spring.datasource.driver-class-name"));
                settings.put(Environment.URL, PROPERTIES_HANDLER.getProperty("spring.datasource.url"));
                settings.put(Environment.USER, PROPERTIES_HANDLER.getProperty("spring.datasource.username"));
                settings.put(Environment.PASS, PROPERTIES_HANDLER.getProperty("spring.datasource.password"));
                settings.put(Environment.DIALECT, PROPERTIES_HANDLER.getProperty("spring.jpa.properties.hibernate.dialect"));
                settings.put(Environment.SHOW_SQL, PROPERTIES_HANDLER.getProperty("spring.jpa.show-sql"));
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.HBM2DDL_AUTO, PROPERTIES_HANDLER.getProperty("spring.jpa.hibernate.ddl-auto"));
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(ActivityInstanceEntity.class);
                configuration.addAnnotatedClass(ExecutionEntity.class);
                configuration.addAnnotatedClass(IncidentEntity.class);
                configuration.addAnnotatedClass(ProcessDefinitionEntity.class);
                configuration.addAnnotatedClass(TaskEntity.class);
                configuration.addAnnotatedClass(UserEntity.class);
                configuration.addAnnotatedClass(VariableInstanceEntity.class);
                configuration.addAnnotatedClass(VariableHistoryEntity.class);
                configuration.addAnnotatedClass(ActivityInstanceEntity.class);
                configuration.addAnnotatedClass(ProcessDefinitionEntity.class);
                configuration.addAnnotatedClass(ProcessInstanceEntity.class);
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}