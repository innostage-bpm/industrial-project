package innostage.camunda.utils;

import innostage.camunda.BpmnAdaptorApplication;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Logger;

@Component
public class DataBaseEnumsHandler {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private static final PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    private final Properties prop;
    private Connection con;

    public DataBaseEnumsHandler() {
        this.prop = new Properties();
        try (InputStream inputStream = BpmnAdaptorApplication.class.getResourceAsStream("/application.properties")) {
            prop.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace(System.out);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        String url = PROPERTIES_HANDLER.getProperty("spring.datasource.url");
        String user = PROPERTIES_HANDLER.getProperty("spring.datasource.username");
        String password = PROPERTIES_HANDLER.getProperty("spring.datasource.password");
        try {
            con = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        createEnums();
    }

    private void createEnums() {
        createEnum("variable_state");
        createEnum("variable_type");
        createEnum("process_instance_state");
        createEnum("activity_instance_type");
    }

    // Does not checking if the enums already presented in the DB
    private void createEnum(String enumName) {
        try (Statement st = con.createStatement()) {
            switch (enumName) {
                case "variable_state":
                    st.executeUpdate(CREATE_VARIABLES_STATE);
                    break;
                case "variable_type":
                    st.executeUpdate(CREATE_VARIABLES_TYPE);
                    break;
                case "process_instance_state":
                    st.executeUpdate(CREATE_PROCESS_INSTANCE_TYPE);
                    break;
                case "activity_instance_type":
                    st.executeUpdate(CREATE_ACTIVITY_INSTANCE_TYPE);
            }
        } catch (SQLException ex) {
            LOGGER.severe("Error in creating enums");
        }
    }

    private static final String CREATE_VARIABLES_STATE = "create type variable_state as enum ('CREATED', 'DELETED')";
    private static final String CREATE_VARIABLES_TYPE = "create type variable_type as enum ('Boolean', 'Bytes', 'Short', 'Integer', 'Long'\n" +
            "    ,'Double', 'Date', 'String', 'Null', 'File', 'Object', 'Json', 'Xml')";
    private static final String CREATE_PROCESS_INSTANCE_TYPE = "create type process_instance_state as enum ('ACTIVE', 'SUSPENDED', 'COMPLETED', 'EXTERNALLY_TERMINATED', 'INTERNALLY_TERMINATED');";
    private static final String CREATE_ACTIVITY_INSTANCE_TYPE = "create type activity_instance_type as enum (\n" +
            "    'cancelBoundaryCatch',\n" +
            "    'compensationBoundaryCatch',\n" +
            "    'boundaryConditional',\n" +
            "    'boundaryError',\n" +
            "    'boundaryEscalation',\n" +
            "    'boundaryMessage',\n" +
            "    'boundarySignal',\n" +
            "    'boundaryTimer',\n" +
            "    'callActivity',\n" +
            "    'cancelEndEvent',\n" +
            "    'compensationEndEvent',\n" +
            "    'errorEndEvent',\n" +
            "'escalationEndEvent',\n" +
            "'messageEndEvent',\n" +
            "'noneEndEvent',\n" +
            "'signalEndEvent',\n" +
            "'terminateEndEvent',\n" +
            "'complexGateway',\n" +
            "'eventBasedGateway',\n" +
            "'exclusiveGateway',\n" +
            "'inclusiveGateway',\n" +
            "'parallelGateway',\n" +
            "'intermediateCatchEvent',\n" +
            "'intermediateCompensationThrowEvent',\n" +
            "'intermediateConditional',\n" +
            "'intermediateEscalationThrowEvent',\n" +
            "'intermediateLinkCatch',\n" +
            "'intermediateMessageCatch',\n" +
            "'intermediateMessageThrowEvent',\n" +
            "'intermediateNoneThrowEvent',\n" +
            "'intermediateSignalCatch',\n" +
            "'intermediateSignalThrow',\n" +
            "'intermediateThrowEvent',\n" +
            "'intermediateTimer',\n" +
            "'multiInstanceBody',\n" +
            "'startEvent',\n" +
            "'compensationStartEvent',\n" +
            "'conditionalStartEvent',\n" +
            "'errorStartEvent',\n" +
            "'escalationStartEvent',\n" +
            "'messageStartEvent',\n" +
            "'signalStartEvent',\n" +
            "'startTimerEvent',\n" +
            "'subProcess',\n" +
            "'adHocSubProcess',\n" +
            "'task',\n" +
            "'businessRuleTask',\n" +
            "'manualTask',\n" +
            "'receiveTask',\n" +
            "'scriptTask',\n" +
            "'sendTask',\n" +
            "'serviceTask',\n" +
            "'userTask',\n" +
            "'transaction'\n" +
            ")";

}

