package innostage.camunda.services.exceptions;


public class EngineAddressIsInvalidException extends RuntimeException {

	public EngineAddressIsInvalidException(String engineAddress) {
		super("Engine address " + engineAddress + " is not valid!");
	}
}
