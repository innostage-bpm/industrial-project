package innostage.camunda.services.exceptions;

public class IncidentNotificationIntervalException extends RuntimeException {

    public IncidentNotificationIntervalException(long interval) {
        super("incident notification interval = " + interval + " is not valid!");
    }
}

