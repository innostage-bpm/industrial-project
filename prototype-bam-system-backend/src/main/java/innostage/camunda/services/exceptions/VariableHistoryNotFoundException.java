package innostage.camunda.services.exceptions;

public class VariableHistoryNotFoundException extends RuntimeException {

	VariableHistoryNotFoundException(String id) {
		super("Could not find variable with id = " + id);
	}
}
