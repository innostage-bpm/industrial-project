package innostage.camunda.services.exceptions;

public class VariableInstanceNotFoundException extends RuntimeException {

	VariableInstanceNotFoundException(String id) {
		super("Could not find variable with id = " + id);
	}
}
