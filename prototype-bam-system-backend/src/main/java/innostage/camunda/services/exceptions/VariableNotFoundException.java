package innostage.camunda.services.exceptions;

public class VariableNotFoundException extends RuntimeException {

	VariableNotFoundException(String id) {
		super("Could not find variable with id = " + id);
	}
}
