package innostage.camunda.services.exceptions;

public class TaskNotFoundException extends RuntimeException {

	public TaskNotFoundException(String id) {
		super("Could not find task with id = " + id);
	}
}
