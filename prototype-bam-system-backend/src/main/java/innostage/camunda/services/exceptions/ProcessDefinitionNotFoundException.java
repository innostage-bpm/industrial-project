package innostage.camunda.services.exceptions;

public class ProcessDefinitionNotFoundException extends RuntimeException {

    public ProcessDefinitionNotFoundException(String id) {
        super("Could not find process definition with such id:  " + id);
    }
}