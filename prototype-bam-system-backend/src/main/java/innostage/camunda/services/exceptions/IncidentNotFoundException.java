package innostage.camunda.services.exceptions;

public class IncidentNotFoundException extends RuntimeException {

	public IncidentNotFoundException(String id) {
		super("Could not find incident with id = " + id);
	}
}
