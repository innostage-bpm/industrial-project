package innostage.camunda.services.exceptions;

public class ProcessInstanceNotFoundException extends RuntimeException {

	public ProcessInstanceNotFoundException(String id) {
		super("Could not find process instance with id = " + id);
	}

}
