package innostage.camunda.services.exceptions;

public class SyncPeriodTimeIsInvalidException extends RuntimeException {

	public SyncPeriodTimeIsInvalidException(String engineAddress) {
		super("Sync period =  " + engineAddress + " is not valid!");
	}
}
