package innostage.camunda.services.exceptions;

public class ActivityInstanceNotFoundException extends RuntimeException {

	public ActivityInstanceNotFoundException(String id) {
		super("Could not find activity instance with id = " + id);
	}
}
