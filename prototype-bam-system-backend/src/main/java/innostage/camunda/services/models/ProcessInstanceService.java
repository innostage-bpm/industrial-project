package innostage.camunda.services.models;


import innostage.camunda.models.ActivityInstanceEntity;
import innostage.camunda.models.ProcessInstanceEntity;

import java.util.List;

public interface ProcessInstanceService {

    List<ProcessInstanceEntity> findAll();
    ProcessInstanceEntity getByID(String id);
    void save(ProcessInstanceEntity processInstance);
    void deleteById(String id);
    void deleteAll();
    long calcDurationInMillisById(List<ActivityInstanceEntity> activities, String id);
}
