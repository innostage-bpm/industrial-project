package innostage.camunda.services.models;

import innostage.camunda.models.IncidentEntity;

import java.util.List;

public interface IncidentService {

    List<IncidentEntity> findAll();
    IncidentEntity getByID(String id);
    void save(IncidentEntity incident);
    void deleteById(String id);
    void deleteAll();
    List<IncidentEntity> getIncidentUpdates(long seconds);
}
