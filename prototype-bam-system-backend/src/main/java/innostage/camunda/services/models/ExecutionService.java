package innostage.camunda.services.models;


import innostage.camunda.models.ExecutionEntity;

import java.util.List;

public interface ExecutionService {

    List<ExecutionEntity> findAll();
    ExecutionEntity getByID(String id);
    void save(ExecutionEntity execution);
    void deleteById(String id);
    void deleteAll();

}
