package innostage.camunda.services.models;

import innostage.camunda.models.VariableHistoryEntity;

import java.util.List;

public interface VariableHistoryService {

    List<VariableHistoryEntity> findAll();
    VariableHistoryEntity getByID(String id);
    void save(VariableHistoryEntity variable);
    void deleteById(String id);
    void deleteAll();
}
