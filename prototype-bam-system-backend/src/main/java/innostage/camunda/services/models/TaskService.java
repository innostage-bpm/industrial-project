package innostage.camunda.services.models;


import innostage.camunda.models.TaskEntity;

import java.util.List;

public interface TaskService {

    List<TaskEntity> findAll();
    TaskEntity getByID(String id);
    void save(TaskEntity task);
    void deleteById(String id);
    void deleteAll();
}
