package innostage.camunda.services.models;


import innostage.camunda.models.UserEntity;

import java.util.List;

public interface UserService {

    List<UserEntity> findAll();
    UserEntity getByID(String id);
    void save(UserEntity user);
    void deleteById(String id);
    void deleteAll();
}
