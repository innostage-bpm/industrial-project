package innostage.camunda.services.models;


import innostage.camunda.models.VariableInstanceEntity;

import java.util.List;

public interface VariableInstanceService {

    List<VariableInstanceEntity> findAll();
    VariableInstanceEntity getByID(String id);
    void save(VariableInstanceEntity variable);
    void deleteById(String id);
    void deleteAll();
}
