package innostage.camunda.services.models;


import innostage.camunda.models.ProcessDefinitionEntity;

import java.util.List;

public interface ProcessDefinitionService {

    List<ProcessDefinitionEntity> findAll();
    ProcessDefinitionEntity getByID(String id);
    ProcessDefinitionEntity save(ProcessDefinitionEntity processDefinition);
    void deleteById(String id);
    void deleteAll();
	String getXMLByID(String id);
}
