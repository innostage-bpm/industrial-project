package innostage.camunda.services.models;

import innostage.camunda.models.ActivityInstanceEntity;

import java.util.List;

public interface ActivityInstanceService {

    List<ActivityInstanceEntity> findAll();
    ActivityInstanceEntity getByID(String id);
    void save(ActivityInstanceEntity activityInstance);
    void deleteById(String id);
    void deleteAll();

}
