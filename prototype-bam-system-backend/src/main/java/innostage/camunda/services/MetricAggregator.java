package innostage.camunda.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import innostage.camunda.models.*;
import innostage.camunda.services.impl.BpmAdaptorImpl;
import innostage.camunda.services.models.*;
import innostage.camunda.utils.HibernateUtil;
import innostage.camunda.utils.VariableHistoryDeserializer;
import innostage.camunda.utils.VariableInstanceDeserializer;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;


@Component
public class MetricAggregator {

    private static final double FROM_NANOS_TO_SECONDS = 1000000000.0;
    private final Logger LOGGER = Logger.getLogger(MetricAggregator.class.getName());
    private Timestamp lastSyncTime;

    final
    ProcessDefinitionService processDefinitionService;
    final
    ProcessInstanceService processInstanceService;
    final
    ActivityInstanceService activityInstanceService;
    final
    TaskService taskService;
    final
    UserService userService;
    final
    IncidentService incidentService;
    final
    VariableHistoryService variableHistoryService;
    final
    VariableInstanceService variableInstanceService;


	public MetricAggregator(ProcessDefinitionService processDefinitionService, ActivityInstanceService activityInstanceService, TaskService taskService, UserService userService, IncidentService incidentService, VariableHistoryService variableHistoryService, VariableInstanceService variableInstanceService,  ProcessInstanceService processInstanceService) {
        this.processDefinitionService = processDefinitionService;
        this.activityInstanceService = activityInstanceService;
        this.taskService = taskService;
        this.userService = userService;
        this.incidentService = incidentService;
        this.variableHistoryService = variableHistoryService;
        this.variableInstanceService = variableInstanceService;
        this.processInstanceService = processInstanceService;

    }

    public void updateData() {

        HashMap<String,String> data = null;
        try {
            data = requestData();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.severe("An error during requesting data from BPMN engine");
        }
        parseAndSaveData(data);

    }

    public HashMap<String, String> requestData() throws Exception {
        HashMap<String, String> data = new HashMap<>();
        LOGGER.info("staring sync with the engine");
        long startTime = System.nanoTime();

        BpmAdaptor adaptor = new BpmAdaptorImpl();
        data.put("process_definitions", adaptor.getProcessDefinitions());
        lastSyncTime = new Timestamp(System.currentTimeMillis());
        data.put("process_instances", adaptor.getProcessInstances());
        data.put("tasks", adaptor.getTasks());
        data.put("activity_instances", adaptor.getActivityInstances());
        data.put("executions", adaptor.getExecutions());
        data.put("users", adaptor.getUsers());
        data.put("variable_instances", adaptor.getVariableInstances());
        data.put("variable_details", adaptor.getVariableDetails());
        data.put("incidents", adaptor.getIncidents());
        loadProcessDefinitionXMLs(data, adaptor);

        long endTime = System.nanoTime();
        double duration = (endTime - startTime) / FROM_NANOS_TO_SECONDS;
        LOGGER.info("sync with the engine finished");
        LOGGER.info("sync duration (sec): " + duration);
        return data;
    }

    private void loadProcessDefinitionXMLs(HashMap<String, String> data, BpmAdaptor adaptor) throws Exception {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Type processDefType = new TypeToken<List<ProcessDefinitionEntity>>() {}.getType();
        List<ProcessDefinitionEntity> processDefinitions = gson.fromJson(data.get("process_definitions"),processDefType);
        for (ProcessDefinitionEntity processDefinition : processDefinitions) {
            Properties xmlField = gson.fromJson(adaptor.getProcessDefinitionXML(processDefinition.getId()), Properties.class);
            String xml = xmlField.getProperty("bpmn20Xml");
            data.put(processDefinition.getId(), xml) ;
        }
    }

    private void parseAndSaveData(HashMap<String, String> data) {

        Type processDefType = new TypeToken<List<ProcessDefinitionEntity>>() {}.getType();
        Type processInstanceType = new TypeToken<List<ProcessInstanceEntity>>() {}.getType();
        Type userType = new TypeToken<List<UserEntity>>() {}.getType();
        Type activityType = new TypeToken<List<ActivityInstanceEntity>>() {}.getType();
        Type executionType = new TypeToken<List<ExecutionEntity>>() {}.getType();
        Type taskType = new TypeToken<List<TaskEntity>>() {}.getType();
        Type incidentType = new TypeToken<List<IncidentEntity>>() {}.getType();
        Type variableInstanceEntity = new TypeToken<ArrayList<VariableInstanceEntity>>(){}.getType();
        Type variableHistoryEntity = new TypeToken<ArrayList<VariableHistoryEntity>>(){}.getType();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(VariableInstanceEntity.class, new VariableInstanceDeserializer());
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableHistoryDeserializer());
        Gson gson = gsonBuilder.create();

        List<ProcessDefinitionEntity> processDefinitions = gson.fromJson(data.get("process_definitions"),processDefType);
        List<ProcessInstanceEntity> processInstances = gson.fromJson(data.get("process_instances"),processInstanceType);
        List<UserEntity> users = gson.fromJson(data.get("users"),userType);
        List<ActivityInstanceEntity> activities = gson.fromJson(data.get("activity_instances"),activityType);
        List<ExecutionEntity> executions = gson.fromJson(data.get("executions"),executionType);
        List<TaskEntity> tasks = gson.fromJson(data.get("tasks"),taskType);
        List<IncidentEntity> incidents = gson.fromJson(data.get("incidents"),incidentType);
        List<VariableInstanceEntity> variable_instances = gson.fromJson(data.get("variable_instances"),variableInstanceEntity);
        List<VariableHistoryEntity> variables_details = gson.fromJson(data.get("variable_details"),variableHistoryEntity);

        Session session = HibernateUtil.getSessionFactory().openSession();
        setXMLsForEachProcessDefinition(processDefinitions, data);

        saveUsers(users);
        saveProcessDefinitions(processDefinitions);
        saveProcessAndActivityInstancesWithExecutions(processInstances, activities, executions, session);
        saveTasks(tasks);
        saveIncidents(incidents);
        saveVariableInstances(variable_instances);
        saveVariableHistory(variables_details);
    }

    private void setXMLsForEachProcessDefinition(List<ProcessDefinitionEntity> processDefinitions, HashMap<String, String> data) {
        for (ProcessDefinitionEntity processDefinition : processDefinitions) {
            processDefinition.setSchemaXml(data.get(processDefinition.getId()));
        }
    }

    private void saveVariableHistory(List<VariableHistoryEntity> variables_details) {
        for (VariableHistoryEntity variable_his : variables_details) {
            if (variable_his != null) {
                variableHistoryService.save(variable_his);
            }
        }
    }

    private void saveVariableInstances(List<VariableInstanceEntity> variable_instances) {
        for (VariableInstanceEntity variable : variable_instances) {
            if (variable != null) {
                variableInstanceService.save(variable);
            }
        }
    }

    private void saveIncidents(List<IncidentEntity> incidents) {
        for (IncidentEntity incident : incidents) {
            incidentService.save(incident);
        }
    }

    private void saveTasks(List<TaskEntity> tasks) {
        for (TaskEntity task : tasks) {
            taskService.save(task);
        }
    }

    private void saveProcessAndActivityInstancesWithExecutions(List<ProcessInstanceEntity> processInstances, List<ActivityInstanceEntity> activities, List<ExecutionEntity> executions, Session session) {
        for (ProcessInstanceEntity processInstance : processInstances) {

            session.beginTransaction();
            processInstance.setLastSyncTime(lastSyncTime);
            long duration = processInstanceService.calcDurationInMillisById(activities, processInstance.getId());
            processInstance.setDurationInMillis(duration);
            session.saveOrUpdate(processInstance);

            for (ExecutionEntity execution : executions) {
                if (execution.getProcessInstanceId().equals(processInstance.getId())) {
                    session.saveOrUpdate(execution);
                }
            }

            for (ActivityInstanceEntity activity : activities) {
                activity.setParentActivityInstanceId(activity.getId());
                if (activity.getProcessInstanceId().equals(processInstance.getId())) {
                    session.saveOrUpdate(activity);
                }
            }

            session.getTransaction().commit();
        }
        session.close();
    }

    private void saveProcessDefinitions(List<ProcessDefinitionEntity> processDefinitions) {
        for (ProcessDefinitionEntity processDefinition : processDefinitions) {
            processDefinitionService.save(processDefinition);
        }
    }

    private void saveUsers(List<UserEntity> users) {
        for (UserEntity user : users) {
            userService.save(user);
        }
    }

    public void updateData(String process_instance_id) {
        HashMap<String,String> data = null;
        try {
            data = requestData(process_instance_id);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.severe("An error during requesting data from BPMN engine");
        }
        parseAndSaveData(data);
    }

    private HashMap<String, String> requestData(String process_instance_id) throws Exception {
        HashMap<String, String> data = new HashMap<>();
        LOGGER.info("staring process sync with the engine");
        long startTime = System.nanoTime();
        BpmAdaptor adaptor = new BpmAdaptorImpl();
        data.put("process_definitions", adaptor.getProcessDefinitions());
        lastSyncTime = new Timestamp(System.currentTimeMillis());
        data.put("process_instances", adaptor.getProcessInstances(process_instance_id));
        data.put("tasks", adaptor.getTasks(process_instance_id));
        data.put("activity_instances", adaptor.getActivityInstances(process_instance_id));
        data.put("executions", adaptor.getExecutions(process_instance_id));
        data.put("users", adaptor.getUsers());
        data.put("variable_instances", adaptor.getVariableInstances(process_instance_id));
        data.put("variable_details", adaptor.getVariableDetails(process_instance_id));
        data.put("incidents", adaptor.getIncidents(process_instance_id));
        loadProcessDefinitionXMLs(data, adaptor);
        long endTime = System.nanoTime();
        double duration = (endTime - startTime) / FROM_NANOS_TO_SECONDS;
        LOGGER.info("one process sync with the engine finished");
        LOGGER.info("one process sync duration (sec): " + duration);
        return data;
    }
}
