package innostage.camunda.services;

public interface BpmAdaptor {

    //Process Definition
    String getProcessDefinitions() throws Exception;
    String getProcessDefinitionXML(String processDefId) throws Exception;

    //Process Instance
    String getProcessInstances() throws Exception;
    String getProcessInstances(String processInstId) throws Exception;

    //Task
    String getTasks() throws Exception;
    String getTasks(String processInstId) throws Exception;

    //ActivityInstance
    String getActivityInstances() throws Exception;
    String getActivityInstances(String processInstId) throws Exception;

    //Execution
    String getExecutions() throws Exception;
    String getExecutions(String processInstId) throws Exception;

    //User
    String getUsers() throws Exception;

    //Variable
    String getVariableInstances() throws Exception;
    String getVariableInstances(String processInstId) throws Exception;
    String getVariableDetails() throws Exception;
    String getVariableDetails(String processInstId) throws Exception;

    //Incident
    String getIncidents() throws Exception;
    String getIncidents(String processInstId) throws Exception;

}
