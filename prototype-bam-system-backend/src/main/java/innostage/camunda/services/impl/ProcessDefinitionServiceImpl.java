package innostage.camunda.services.impl;


import innostage.camunda.models.ProcessDefinitionEntity;
import innostage.camunda.repository.ProcessDefinitionRepository;
import innostage.camunda.services.exceptions.ProcessDefinitionNotFoundException;
import innostage.camunda.services.models.ProcessDefinitionService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessDefinitionServiceImpl implements ProcessDefinitionService {

    @Autowired
    private ProcessDefinitionRepository processDefinitionRepository;

    @Override
    public List<ProcessDefinitionEntity> findAll() {
        return Lists.newArrayList(processDefinitionRepository.findAll());
    }

    @Override
    public ProcessDefinitionEntity getByID(String id) {
        return processDefinitionRepository.findById(id).orElseThrow(() -> new ProcessDefinitionNotFoundException(id));
    }

    @Override
    public ProcessDefinitionEntity save(ProcessDefinitionEntity processDefinition) { processDefinitionRepository.save(processDefinition);
        return processDefinition;
    }

    @Override
    public void deleteById(String id) { processDefinitionRepository.deleteById(id); }

    @Override
    public void deleteAll() { processDefinitionRepository.deleteAll();}

    @Override
    public String getXMLByID(String id) {
        return getByID(id).getSchemaXml();
    }
}
