package innostage.camunda.services.impl;


import innostage.camunda.services.BpmAdaptor;
import innostage.camunda.utils.PropertiesHandler;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

@Service
public class BpmAdaptorImpl implements BpmAdaptor {

    private final OkHttpClient httpClient = new OkHttpClient();
    private static final String INCLUDE_DELETED_VARIABLES_PARAM = "includeDeleted";
    private static final String PROCESS_INSTANCE_PARAM = "processInstanceId";
    private static final PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();

    private String sendRequest(String endPoint, HashMap<String, String> params) throws Exception {

        URL url = buildUrl(endPoint, params);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("content-type", "application/json")
                .get()
                .build();

        Response response = httpClient.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        return response.body().string();
    }

    private URL buildUrl(String endPoint, HashMap<String, String> params) {
        HttpUrl.Builder builder = new HttpUrl.Builder();
        int port = PROPERTIES_HANDLER.getEnginePort();
        builder.scheme("http")
                .host(PROPERTIES_HANDLER.getEngineHost())
                .addPathSegments("engine-rest")
                .addPathSegments(endPoint);

        if (port > 0) {
            builder.port(port);
        }

        if (params != null && !params.isEmpty()) {
            params.forEach(builder::addQueryParameter);
        }

        return builder.build().url();
    }

    @Override
    public String getProcessDefinitions() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("process_definitions"), null);
    }

    @Override
    public String getProcessDefinitionXML(String processDefId) throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("process_definitions") + "/" + processDefId + "/xml", null);
    }

    @Override
    public String getProcessInstances() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("process_instances"), null);
    }

    @Override
    public String getProcessInstances(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("process_instances"), params);
    }

    @Override
    public String getTasks() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("tasks"), null);
    }

    @Override
    public String getTasks(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("tasks"), params);
    }

    @Override
    public String getActivityInstances() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("activity_instances"), null);
    }

    @Override
    public String getActivityInstances(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("activity_instances"), params);
    }

    @Override
    public String getExecutions() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("executions"), null);
    }

    @Override
    public String getExecutions(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("executions"), params);
    }

    @Override
    public String getUsers() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("users"), null);
    }

    @Override
    public String getVariableInstances() throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(INCLUDE_DELETED_VARIABLES_PARAM, "true");
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("variable_instances"), params);
    }

    @Override
    public String getVariableInstances(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(INCLUDE_DELETED_VARIABLES_PARAM, "true");
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("variable_instances"), params);
    }

    @Override
    public String getVariableDetails() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("variable_details"), null);
    }

    @Override
    public String getVariableDetails(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("variable_details"), params);
    }


    @Override
    public String getIncidents() throws Exception {
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("incidents"), null);
    }

    @Override
    public String getIncidents(String processInstId) throws Exception {
        HashMap<String, String> params = new HashMap<>();
        params.put(PROCESS_INSTANCE_PARAM, processInstId);
        return sendRequest(PROPERTIES_HANDLER.getEndpoint("incidents"), params);
    }
}
