package innostage.camunda.services.impl;

import innostage.camunda.models.ActivityInstanceEntity;
import innostage.camunda.models.ProcessInstanceEntity;
import innostage.camunda.repository.ProcessInstanceRepository;
import innostage.camunda.services.exceptions.ProcessInstanceNotFoundException;
import innostage.camunda.services.models.ProcessInstanceService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessInstanceServiceImpl implements ProcessInstanceService {

    @Autowired
    private ProcessInstanceRepository processInstanceRepository;

    @Override
    public List<ProcessInstanceEntity> findAll() {
        return Lists.newArrayList(processInstanceRepository.findAll());
    }

    @Override
    public ProcessInstanceEntity getByID(String id) {
        return processInstanceRepository.findById(id).orElseThrow(() -> new ProcessInstanceNotFoundException(id));
    }

    @Override
    public void save(ProcessInstanceEntity processInstance) { processInstanceRepository.save(processInstance);}

    @Override
    public void deleteById(String id) { processInstanceRepository.deleteById(id); }

    @Override
    public void deleteAll() { processInstanceRepository.deleteAll(); }

    @Override
    public long calcDurationInMillisById(List<ActivityInstanceEntity> activities, String id) {
        long duration = 0;
        for (ActivityInstanceEntity activityInstanceEntity : activities) {
            if(activityInstanceEntity.getProcessInstanceId().equals(id) && activityInstanceEntity != null) {
                Long activityInstanceDuration = activityInstanceEntity.getDurationInMillis();
                if (activityInstanceDuration != null) {
                    duration += activityInstanceDuration;
                }
            }
        }
        return duration;
    }
}
