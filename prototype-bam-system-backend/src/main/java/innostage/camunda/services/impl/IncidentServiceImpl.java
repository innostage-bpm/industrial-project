package innostage.camunda.services.impl;


import innostage.camunda.models.IncidentEntity;
import innostage.camunda.repository.IncidentRepository;
import innostage.camunda.services.exceptions.IncidentNotFoundException;
import innostage.camunda.services.exceptions.IncidentNotificationIntervalException;
import innostage.camunda.services.models.IncidentService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class IncidentServiceImpl implements IncidentService {

    private static final long SECONDS_TO_MILLISECONDS = 1000;

    @Autowired
    private IncidentRepository incidentRepository;

    @Override
    public List<IncidentEntity> findAll() {
        return Lists.newArrayList(incidentRepository.findAll());
    }

    @Override
    public IncidentEntity getByID(String id) {
        return incidentRepository.findById(id).orElseThrow(() -> new IncidentNotFoundException(id));
    }

    @Override
    public void save(IncidentEntity incident) { incidentRepository.save(incident); }

    @Override
    public void deleteById(String id) { incidentRepository.deleteById(id);}

    @Override
    public void deleteAll() { incidentRepository.deleteAll(); }

    @Override
    public List<IncidentEntity> getIncidentUpdates(long seconds) {
        if(seconds <= 0)
            throw new IncidentNotificationIntervalException(seconds);
        List<IncidentEntity> incidents = findAll();

        //Remove from list of incidents such where the createTime was before than current time - @param seconds
        incidents.removeIf(incidentEntity -> (incidentEntity.getCreateTime().getTime() <=
                new Timestamp(System.currentTimeMillis()).getTime() - seconds * SECONDS_TO_MILLISECONDS));
        return incidents;
    }
}
