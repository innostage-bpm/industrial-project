package innostage.camunda.services.impl;


import innostage.camunda.models.ExecutionEntity;
import innostage.camunda.repository.ExecutionRepository;
import innostage.camunda.services.models.ExecutionService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExecutionServiceImpl implements ExecutionService {

    @Autowired
    private ExecutionRepository executionRepository;

    @Override
    public List<ExecutionEntity> findAll() {
        return Lists.newArrayList(executionRepository.findAll());
    }

    @Override
    public ExecutionEntity getByID(String id) {
        return executionRepository.findById(id).get();
    }

    @Override
    public void save(ExecutionEntity execution) { executionRepository.save(execution); }

    @Override
    public void deleteById(String id) { executionRepository.deleteById(id);  }

    @Override
    public void deleteAll() {  executionRepository.deleteAll(); }

}
