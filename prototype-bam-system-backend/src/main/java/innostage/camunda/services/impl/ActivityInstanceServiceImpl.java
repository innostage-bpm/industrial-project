package innostage.camunda.services.impl;

import innostage.camunda.models.ActivityInstanceEntity;
import innostage.camunda.repository.ActivityInstanceRepository;
import innostage.camunda.services.exceptions.ActivityInstanceNotFoundException;
import innostage.camunda.services.models.ActivityInstanceService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityInstanceServiceImpl implements ActivityInstanceService {

    @Autowired
    private ActivityInstanceRepository activityInstanceRepository;

    @Override
    public List<ActivityInstanceEntity> findAll() {
        return Lists.newArrayList(activityInstanceRepository.findAll());
    }

    @Override
    public ActivityInstanceEntity getByID(String id) {
        return activityInstanceRepository.findById(id).orElseThrow(() -> new ActivityInstanceNotFoundException(id));
    }

    @Override
    public void save(ActivityInstanceEntity activityInstance) { activityInstanceRepository.save(activityInstance); }

    @Override
    public void deleteById(String id) { activityInstanceRepository.deleteById(id); }

    @Override
    public void deleteAll() { activityInstanceRepository.deleteAll(); }
}
