package innostage.camunda.services.impl;

import innostage.camunda.models.VariableInstanceEntity;
import innostage.camunda.repository.VariableInstanceRepository;
import innostage.camunda.services.models.VariableInstanceService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VariableInstanceServiceImpl implements VariableInstanceService {

    @Autowired
    private VariableInstanceRepository variableRepository;

    @Override
    public List<VariableInstanceEntity> findAll() {
        return Lists.newArrayList(variableRepository.findAll());
    }

    @Override
    public VariableInstanceEntity getByID(String id) {
        return variableRepository.findById(id).get();
    }

    @Override
    public void save(VariableInstanceEntity variable) { variableRepository.save(variable); }

    @Override
    public void deleteById(String id) { variableRepository.deleteById(id); }

    @Override
    public void deleteAll() { variableRepository.deleteAll(); }
}
