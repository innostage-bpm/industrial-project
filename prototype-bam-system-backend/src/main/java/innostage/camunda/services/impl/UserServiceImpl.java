package innostage.camunda.services.impl;


import innostage.camunda.models.UserEntity;
import innostage.camunda.repository.UserRepository;
import innostage.camunda.services.exceptions.UserNotFoundException;
import innostage.camunda.services.models.UserService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserEntity> findAll() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    public UserEntity getByID(String id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public void save(UserEntity user) { userRepository.save(user); }

    @Override
    public void deleteById(String id) { userRepository.deleteById(id);}

    @Override
    public void deleteAll() { userRepository.deleteAll(); }
}
