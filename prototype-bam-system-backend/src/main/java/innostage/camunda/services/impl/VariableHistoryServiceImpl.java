package innostage.camunda.services.impl;

import innostage.camunda.models.VariableHistoryEntity;
import innostage.camunda.repository.VariableHistoryRepository;
import innostage.camunda.services.models.VariableHistoryService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VariableHistoryServiceImpl implements VariableHistoryService {

    @Autowired
    private VariableHistoryRepository variableRepository;

    @Override
    public List<VariableHistoryEntity> findAll() {
        return Lists.newArrayList(variableRepository.findAll());
    }

    @Override
    public VariableHistoryEntity getByID(String id) {
        return variableRepository.findById(id).get();
    }

    @Override
    public void save(VariableHistoryEntity variable) { variableRepository.save(variable); }

    @Override
    public void deleteById(String id) { variableRepository.deleteById(id); }

    @Override
    public void deleteAll() { variableRepository.deleteAll(); }
}
