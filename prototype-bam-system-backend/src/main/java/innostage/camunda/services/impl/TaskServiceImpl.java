package innostage.camunda.services.impl;


import com.google.api.client.util.Lists;
import innostage.camunda.models.TaskEntity;
import innostage.camunda.repository.TaskRepository;
import innostage.camunda.services.exceptions.TaskNotFoundException;
import innostage.camunda.services.models.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<TaskEntity> findAll() {
        return Lists.newArrayList(taskRepository.findAll());
    }

    @Override
    public TaskEntity getByID(String id) {

        return taskRepository.findById(id).orElseThrow(() -> new TaskNotFoundException(id));
    }

    @Override
    public void save(TaskEntity task) { taskRepository.save(task); }

    @Override
    public void deleteById(String id) { taskRepository.deleteById(id); }

    @Override
    public void deleteAll() { taskRepository.deleteAll(); }
}
