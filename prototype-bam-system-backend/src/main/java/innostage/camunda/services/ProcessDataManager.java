package innostage.camunda.services;

import innostage.camunda.services.models.*;
import innostage.camunda.utils.PropertiesHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ProcessDataManager {

	@Autowired
	private ProcessDefinitionService processDefinitionService;
	@Autowired
	private ActivityInstanceService activityInstanceService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private IncidentService incidentService;
	@Autowired
	private UserService userService;
	@Autowired
	private VariableHistoryService variableHistoryService;
	@Autowired
	private VariableInstanceService variableInstanceService;
	@Autowired
	private ProcessInstanceService processInstanceService;
	@Autowired
	private PropertiesHandler propertiesHandler;

	private long refreshTickNumber = 0;
	private long tickNumber = 0;

	@Scheduled(fixedRateString = "1000", initialDelay = 30000)
	public void syncData() {
	    // temporary solution, will be removed later!
		if (tickNumber < refreshTickNumber) {
			tickNumber++;
			return;
		}
		else {
			tickNumber = 0;
		}
		refreshTickNumber = propertiesHandler.getSyncTimePeriod();
		MetricAggregator aggregator = new MetricAggregator(processDefinitionService,
				activityInstanceService,
				taskService,
				userService,
				incidentService,
				variableHistoryService,
				variableInstanceService, processInstanceService);
		aggregator.updateData();
	}

	public void updateProcessInstanceData(String process_instance_id) {

		MetricAggregator aggregator = new MetricAggregator(processDefinitionService,
				activityInstanceService,
				taskService,
				userService,
				incidentService,
				variableHistoryService,
				variableInstanceService, processInstanceService);
		aggregator.updateData(process_instance_id);

	}

}
