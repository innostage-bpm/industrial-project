package innostage.camunda.controllers;

import innostage.camunda.models.UserEntity;
import innostage.camunda.services.models.UserService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

    private final UserService service;

    UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/users")
    List<UserEntity> all() {
        return (List<UserEntity>) service.findAll();
    }

    @GetMapping("/users/{id}")
    UserEntity one(@PathVariable String id) {
        return service.getByID(id);
    }

}
