package innostage.camunda.controllers;

import innostage.camunda.models.VariableInstanceEntity;
import innostage.camunda.services.models.VariableInstanceService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class VariableInstanceController {

	private final VariableInstanceService service;

	VariableInstanceController(VariableInstanceService service) {
		this.service = service;
	}

	@GetMapping("/variables")
	List<VariableInstanceEntity> all() {
		return (List<VariableInstanceEntity>) service.findAll();
	}

	@GetMapping("/variables/{id}")
	VariableInstanceEntity one(@PathVariable String id) {
		return service.getByID(id);
	}

}
