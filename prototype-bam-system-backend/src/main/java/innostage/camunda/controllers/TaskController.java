package innostage.camunda.controllers;

import innostage.camunda.models.TaskEntity;
import innostage.camunda.services.models.TaskService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TaskController {

    private final TaskService service;

    TaskController(TaskService service) {
        this.service = service;
    }

    @GetMapping("/tasks")
    List<TaskEntity> all() {
        return (List<TaskEntity>) service.findAll();
    }

    @GetMapping("/tasks/{id}")
    TaskEntity one(@PathVariable String id) {
        return service.getByID(id);
    }

}
