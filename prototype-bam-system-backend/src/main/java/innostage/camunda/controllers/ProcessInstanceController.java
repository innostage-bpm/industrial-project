package innostage.camunda.controllers;

import innostage.camunda.models.ProcessInstanceEntity;
import innostage.camunda.services.models.ProcessInstanceService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProcessInstanceController {

    private final ProcessInstanceService service;

    ProcessInstanceController(ProcessInstanceService service) {
        this.service = service;
    }

    @GetMapping("/processInstances")
    List<ProcessInstanceEntity> all() {
        return (List<ProcessInstanceEntity>) service.findAll();
    }

    @GetMapping("/processInstances/{id}")
    ProcessInstanceEntity one(@PathVariable String id) {
        return service.getByID(id);
    }

}
