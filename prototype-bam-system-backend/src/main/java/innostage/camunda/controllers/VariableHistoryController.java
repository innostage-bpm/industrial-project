package innostage.camunda.controllers;

import innostage.camunda.models.VariableHistoryEntity;
import innostage.camunda.services.models.VariableHistoryService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class VariableHistoryController {

	private final VariableHistoryService service;

	VariableHistoryController(VariableHistoryService service) {
		this.service = service;
	}

	@GetMapping("/variablesHistory")
	List<VariableHistoryEntity> all() {
		return (List<VariableHistoryEntity>) service.findAll();
	}

	@GetMapping("/variablesHistory/{id}")
	VariableHistoryEntity one(@PathVariable String id) {
		return service.getByID(id);
	}

}
