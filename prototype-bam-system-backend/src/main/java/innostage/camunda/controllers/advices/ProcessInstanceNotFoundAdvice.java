package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.ProcessInstanceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ProcessInstanceNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(ProcessInstanceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String processInstanceNotFoundHandler(ProcessInstanceNotFoundException ex) {
		return ex.getMessage();
	}
}
