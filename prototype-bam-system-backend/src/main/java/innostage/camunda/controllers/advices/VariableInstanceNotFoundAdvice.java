package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.VariableInstanceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class VariableInstanceNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(VariableInstanceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String variableInstanceNotFoundHandler(VariableInstanceNotFoundException ex) {
		return ex.getMessage();
	}
}
