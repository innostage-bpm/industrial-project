package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.VariableHistoryNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class VariableHistoryNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(VariableHistoryNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String variableHistoryNotFoundHandler(VariableHistoryNotFoundException ex) {
		return ex.getMessage();
	}
}
