package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.EngineAddressIsInvalidException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class EngineAddressInvalidAdvice {

    @ResponseBody
	@ExceptionHandler(EngineAddressIsInvalidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String engineAddressInvalidHandler(EngineAddressIsInvalidException ex) {
		return ex.getMessage();
	}
}