package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.ActivityInstanceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ActivityInstanceNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(ActivityInstanceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String activityInstanceNotFoundHandler(ActivityInstanceNotFoundException ex) {
		return ex.getMessage();
	}
}
