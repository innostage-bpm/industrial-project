package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.IncidentNotificationIntervalException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class IncidentNotificationIntervalNotValidAdvice {

    @ResponseBody
    @ExceptionHandler(IncidentNotificationIntervalException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String syncPeriodTimeInvalidHandler(IncidentNotificationIntervalException ex) {
        return ex.getMessage();
    }
}