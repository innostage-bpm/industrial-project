package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.SyncPeriodTimeIsInvalidException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class SyncPeriodTimeInvalidAdvice {

    @ResponseBody
	@ExceptionHandler(SyncPeriodTimeIsInvalidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String syncPeriodTimeInvalidHandler(SyncPeriodTimeIsInvalidException ex) {
		return ex.getMessage();
	}
}