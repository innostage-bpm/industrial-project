package innostage.camunda.controllers.advices;

import innostage.camunda.services.exceptions.ProcessDefinitionNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class ProcessDefinitionNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(ProcessDefinitionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String processDefinitionNotFoundHandler(ProcessDefinitionNotFoundException ex) {
        return ex.getMessage();
    }
}
