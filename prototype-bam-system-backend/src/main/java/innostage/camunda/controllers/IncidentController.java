package innostage.camunda.controllers;

import innostage.camunda.models.IncidentEntity;
import innostage.camunda.services.models.IncidentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class IncidentController {

    private final IncidentService service;

    IncidentController(IncidentService service) {
        this.service = service;
    }

    @GetMapping("/incidents")
    List<IncidentEntity> all() {
        return (List<IncidentEntity>) service.findAll();
    }

    @GetMapping("/incidents/{id}")
    IncidentEntity one(@PathVariable String id) {
        return service.getByID(id);
    }

    @GetMapping("/incidents/updates")
    List<IncidentEntity> getNotifications(@RequestParam(defaultValue = "1800") long seconds) {
        return service.getIncidentUpdates(seconds);
    }

}
