package innostage.camunda.controllers;

import innostage.camunda.services.exceptions.EngineAddressIsInvalidException;
import innostage.camunda.utils.PropertiesHandler;
import org.apache.commons.configuration.ConfigurationException;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class EnginePropertiesController {

    private static final PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();

    @RequestMapping(value = "/engine", params = "address")
    public String updateEngineAddress(@RequestParam("address") String engineAddress) throws EngineAddressIsInvalidException, ConfigurationException {
        PROPERTIES_HANDLER.setBpmnEngineAddress(engineAddress);
        return "The engine address was set to: " + PROPERTIES_HANDLER.getBpmnEngineAddress();

    }

    @GetMapping(value = "/engine/address")
    public String getEngineAddress() {
        return PROPERTIES_HANDLER.getBpmnEngineAddress();
    }

    @RequestMapping(value = "/engine", params = "sync")
    public String updateSyncPeriodTime(@RequestParam("sync") String syncPeriodTime) throws ConfigurationException {
        PROPERTIES_HANDLER.setSyncPeriodTime(syncPeriodTime);
        return "The sync period time was set to: " + PROPERTIES_HANDLER.getSyncTimePeriod();

    }

    @GetMapping(value = "/engine/sync")
    public String getEngineSyncTime() {
        return String.valueOf(PROPERTIES_HANDLER.getSyncTimePeriod());
    }

    @GetMapping(value = "/")
    public String printInfo() {
        return "BPM adaptor API for engine that is running on: " + PROPERTIES_HANDLER.getBpmnEngineAddress();
    }
}
