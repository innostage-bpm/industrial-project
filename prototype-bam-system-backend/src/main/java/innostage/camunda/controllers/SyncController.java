package innostage.camunda.controllers;

import innostage.camunda.services.ProcessDataManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class SyncController {

    final
    ProcessDataManager processDataManager;

    public SyncController(ProcessDataManager processDataManager) {
        this.processDataManager = processDataManager;
    }

    @RequestMapping(value="/update/{id}")
    public void updateProcessInstance(@PathVariable String id) {
        processDataManager.updateProcessInstanceData(id);
    }

}
