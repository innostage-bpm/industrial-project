package innostage.camunda.controllers;

import innostage.camunda.models.ActivityInstanceEntity;
import innostage.camunda.services.models.ActivityInstanceService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ActivityInstanceController {

    private final ActivityInstanceService service;

    ActivityInstanceController(ActivityInstanceService service) {
        this.service = service;
    }

    @GetMapping("/activityInstances")
    List<ActivityInstanceEntity> all() {
        return (List<ActivityInstanceEntity>) service.findAll();
    }

    @GetMapping("/activityInstances/{id}")
    ActivityInstanceEntity one(@PathVariable String id) {
        return service.getByID(id);
    }


}
