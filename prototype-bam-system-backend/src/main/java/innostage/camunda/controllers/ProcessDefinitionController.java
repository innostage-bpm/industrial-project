package innostage.camunda.controllers;

import innostage.camunda.models.ProcessDefinitionEntity;
import innostage.camunda.services.models.ProcessDefinitionService;
import org.assertj.core.util.Lists;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProcessDefinitionController {

    private final ProcessDefinitionService service;

    ProcessDefinitionController(ProcessDefinitionService service) {
        this.service = service;
    }

    @GetMapping("/processDefinitions")
    List<ProcessDefinitionEntity> all() {
        return Lists.newArrayList(service.findAll());
    }

    @GetMapping("/processDefinitions/{id}")
    ProcessDefinitionEntity one(@PathVariable String id) {
        return service.getByID(id);
    }

    @RequestMapping(value = "/processDefinitions/xml/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Map xml(@PathVariable String id) {
        return Collections.singletonMap("xml", service.getXMLByID(id));
    }

}
