package innostage.camunda.repository;

import innostage.camunda.models.ProcessInstanceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessInstanceRepository extends CrudRepository<ProcessInstanceEntity, String> {

}
