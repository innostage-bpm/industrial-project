package innostage.camunda.repository;

import innostage.camunda.models.ExecutionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExecutionRepository extends CrudRepository<ExecutionEntity, String> {

}
