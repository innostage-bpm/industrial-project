package innostage.camunda.repository;

import innostage.camunda.models.IncidentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IncidentRepository extends CrudRepository<IncidentEntity, String> {

}
