package innostage.camunda.repository;

import innostage.camunda.models.ProcessDefinitionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessDefinitionRepository extends CrudRepository<ProcessDefinitionEntity, String> {
    
}
