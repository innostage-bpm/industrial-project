package innostage.camunda.repository;

import innostage.camunda.models.VariableInstanceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariableInstanceRepository extends CrudRepository<VariableInstanceEntity, String> {

}
