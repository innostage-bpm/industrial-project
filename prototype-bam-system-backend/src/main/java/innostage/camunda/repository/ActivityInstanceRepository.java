package innostage.camunda.repository;

import innostage.camunda.models.ActivityInstanceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityInstanceRepository extends CrudRepository<ActivityInstanceEntity, String> {

}
