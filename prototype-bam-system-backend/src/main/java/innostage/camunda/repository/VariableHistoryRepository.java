package innostage.camunda.repository;

import innostage.camunda.models.VariableHistoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariableHistoryRepository extends CrudRepository<VariableHistoryEntity, String> {

}
