package innostage.camunda.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "execution", schema = "public", catalog = "adaptor")
public class ExecutionEntity {
    private String id;
    private String processInstanceId;
    private boolean ended;
//    private Collection<ActivityInstanceEntity> activityInstancesById;
    private ProcessInstanceEntity processInstanceByProcessInstanceId;

    public ExecutionEntity() {
    }

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "process_instance_id", nullable = false, length = 255)
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Basic
    @Column(name = "ended", nullable = false)
    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExecutionEntity that = (ExecutionEntity) o;
        return ended == that.ended &&
                Objects.equals(id, that.id) &&
                Objects.equals(processInstanceId, that.processInstanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, processInstanceId, ended);
    }

//    @OneToMany(mappedBy = "executionByExecutionId")
//    public Collection<ActivityInstanceEntity> getActivityInstancesById() {
//        return activityInstancesById;
//    }
//
//    public void setActivityInstancesById(Collection<ActivityInstanceEntity> activityInstancesById) {
//        this.activityInstancesById = activityInstancesById;
//    }

    @ManyToOne
    @JoinColumn(name = "process_instance_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ProcessInstanceEntity getProcessInstanceByProcessInstanceId() {
        return processInstanceByProcessInstanceId;
    }

    public void setProcessInstanceByProcessInstanceId(ProcessInstanceEntity processInstanceByProcessInstanceId) {
        this.processInstanceByProcessInstanceId = processInstanceByProcessInstanceId;
    }

}
