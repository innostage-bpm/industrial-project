package innostage.camunda.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import innostage.camunda.models.enums.VariableState;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "variable_instance", schema = "public", catalog = "adaptor")
public class VariableInstanceEntity {
    private String id;
    private String name;
    private VariableState state;
    private Timestamp createTime;
    private String executionId;
    private String processInstanceId;
    private Collection<VariableHistoryEntity> variableHistoriesById;
    @JsonIgnore
    private ProcessInstanceEntity processInstanceByProcessInstanceId;

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "state", nullable = false, columnDefinition = "variable_state")
    @Type( type = "pgsql_enum" )
    @Enumerated(EnumType.STRING)
    public VariableState getState() {
        return state;
    }

    public void setState(VariableState state) {
        this.state = state;
    }

    @Basic
    @Column(name = "create_time", nullable = false)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VariableInstanceEntity that = (VariableInstanceEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(state, that.state) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, state, createTime);
    }

    @OneToMany(mappedBy = "variableInstanceByVariableInstanceId")
    public Collection<VariableHistoryEntity> getVariableHistoriesById() {
        return variableHistoriesById;
    }

    public void setVariableHistoriesById(Collection<VariableHistoryEntity> variableHistoriesById) {
        this.variableHistoriesById = variableHistoriesById;
    }

    @ManyToOne
    @JoinColumn(name = "process_instance_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ProcessInstanceEntity getProcessInstanceByProcessInstanceId() {
        return processInstanceByProcessInstanceId;
    }

    public void setProcessInstanceByProcessInstanceId(ProcessInstanceEntity processInstanceByProcessInstanceId) {
        this.processInstanceByProcessInstanceId = processInstanceByProcessInstanceId;
    }

//    @ManyToOne
//    @JoinColumn(name = "execution_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
//    public ExecutionEntity getExecutionByExecutionId() {
//        return executionByExecutionId;
//    }
//
//    public void setExecutionByExecutionId(ExecutionEntity executionByExecutionId) {
//        this.executionByExecutionId = executionByExecutionId;
//    }

    @Basic
    @Column(name = "execution_id", nullable = false)
    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    @Basic
    @Column(name = "process_instance_id", nullable = false)
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }
}
