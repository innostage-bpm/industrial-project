package innostage.camunda.models;


import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import innostage.camunda.models.enums.VariableType;
import innostage.camunda.models.sql_types.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.postgresql.util.PGobject;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "variable_history", schema = "public", catalog = "adaptor")

@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
})

public class VariableHistoryEntity {
    private String id;
    private Timestamp time;
    private String timeZone;
    private String scope;
    private VariableType type;
    private PGobject value;
    private PGobject valueInfo;
    private String taskId;
    @JsonIgnore
    private VariableInstanceEntity variableInstanceByVariableInstanceId;
    private String variableInstanceId;
    private TaskEntity taskByTaskId;

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "time", nullable = false)
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "scope", nullable = false, length = 255)
    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Basic
    @Column(name = "type", nullable = false, columnDefinition = "variable_type")
    @Type( type = "pgsql_enum" )
    @Enumerated(EnumType.STRING)
    public VariableType getType() {
        return type;
    }

    public void setType(VariableType type) {
        this.type = type;
    }

    @Type(type = "jsonb")
    @Column(name = "value", nullable = true,columnDefinition = "jsonb")
    public PGobject  getValue() {
        return value;
    }

    public void setValue(PGobject  value) {
        this.value = value;
    }

    @Type(type = "jsonb")
    @Column(name = "value_info", nullable = true ,columnDefinition = "jsonb")
    public PGobject  getValueInfo() {
        return valueInfo;
    }

    public void setValueInfo(PGobject valueInfo) {
        this.valueInfo = valueInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VariableHistoryEntity that = (VariableHistoryEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(time, that.time) &&
                Objects.equals(scope, that.scope) &&
                Objects.equals(type, that.type) &&
                Objects.equals(value, that.value) &&
                Objects.equals(valueInfo, that.valueInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time, scope, type, value, valueInfo);
    }

    @ManyToOne
    @JoinColumn(name = "variable_instance_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public VariableInstanceEntity getVariableInstanceByVariableInstanceId() {
        return variableInstanceByVariableInstanceId;
    }

    public void setVariableInstanceByVariableInstanceId(VariableInstanceEntity variableInstanceByVariableInstanceId) {
        this.variableInstanceByVariableInstanceId = variableInstanceByVariableInstanceId;
    }

    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "id", nullable = true)
    public TaskEntity getTaskByTaskId() {
        return taskByTaskId;
    }

    public void setTaskByTaskId(TaskEntity taskByTaskId) {
        this.taskByTaskId = taskByTaskId;
    }

    @Basic
    @Column(name = "task_id", insertable = false, updatable = false)
    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "variable_instance_id", nullable = true)
    public String getVariableInstanceId() {
        return variableInstanceId;
    }

    public void setVariableInstanceId(String variableInstanceId) {
        this.variableInstanceId = variableInstanceId;
    }

    @Basic
    @Column(name = "time_zone", nullable = true)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
