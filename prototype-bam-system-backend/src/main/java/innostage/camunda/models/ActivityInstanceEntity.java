package innostage.camunda.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import innostage.camunda.models.enums.ActivityInstanceType;
import innostage.camunda.models.sql_types.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "activity_instance", schema = "public", catalog = "adaptor")
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class ActivityInstanceEntity {
    private String id;
    private String parentActivityInstanceId;
    private ActivityInstanceType activityType;
    private String activityId;
    private String activityName;
    private String processInstanceId;
    private Timestamp startTime;
    private Timestamp endTime;
    private Timestamp removalTime;
    private boolean canceled;
    private String executionId;
    private ActivityInstanceEntity activityInstanceById;
    private Long durationInMillis;
    @JsonIgnore private ProcessInstanceEntity processInstanceByProcessInstanceId;
//    private ExecutionEntity executionByExecutionId;
    private Collection<IncidentEntity> incidentsById;
    private ProcessInstanceEntity processInstanceById;
    private TaskEntity taskById;
    private Collection<TaskEntity> tasksById;
    @JsonIgnore
    private Collection<VariableInstanceEntity> variablesById;


    public ActivityInstanceEntity() {
    }

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "parent_activity_instance_id", nullable = true, length = 255)
    public String getParentActivityInstanceId() {
        return parentActivityInstanceId;
    }

    public void setParentActivityInstanceId(String parentActivityInstanceId) {
        this.parentActivityInstanceId = parentActivityInstanceId;
    }

    @Column(name = "type", columnDefinition = "activity_instance_type")
    @Type(type = "pgsql_enum")
    @Enumerated(EnumType.STRING)
    public ActivityInstanceType getType() {
        return activityType;
    }

    public void setType(ActivityInstanceType type) {
        this.activityType = type;
    }

    @Basic
    @Column(name = "activity_id", nullable = false, length = 255)
    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    @Basic
    @Column(name = "name", length = 255)
    public String getName() {
        return activityName;
    }

    public void setName(String name) {
        this.activityName = name;
    }

    @Basic
    @Column(name = "process_instance_id", nullable = false, length = 255)
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }


    @Basic
    @Column(name = "duration_in_millis", nullable = true)
    public Long getDurationInMillis() {
        return durationInMillis;
    }

    public void setDurationInMillis(Long durationInMillis) {
        this.durationInMillis = durationInMillis;
    }

    @Basic
    @Column(name = "removal_time", nullable = true)
    public Timestamp getRemovalTime() {
        return removalTime;
    }

    public void setRemovalTime(Timestamp removalTime) {
        this.removalTime = removalTime;
    }

    @Basic
    @Column(name = "canceled", nullable = false)
    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Basic
    @Column(name = "execution_id", nullable = false, length = 255)
    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityInstanceEntity that = (ActivityInstanceEntity) o;
        return canceled == that.canceled &&
                Objects.equals(id, that.id) &&
                Objects.equals(parentActivityInstanceId, that.parentActivityInstanceId) &&
                Objects.equals(activityType, that.activityType) &&
                Objects.equals(activityId, that.activityId) &&
                Objects.equals(activityName, that.activityName) &&
                Objects.equals(processInstanceId, that.processInstanceId) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(removalTime, that.removalTime) &&
                Objects.equals(executionId, that.executionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, parentActivityInstanceId, activityType, activityId, activityName, processInstanceId, startTime, endTime, removalTime, canceled, executionId);
    }

   /* @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "parent_activity_instance_id", nullable = false, insertable = false, updatable = false)
    public ActivityInstanceEntity getActivityInstanceById() {
        return activityInstanceById;
    }
*/
/*    public void setActivityInstanceById(ActivityInstanceEntity activityInstanceById) {
        this.activityInstanceById = activityInstanceById;
    }*/

    @ManyToOne
    @JoinColumn(name = "process_instance_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ProcessInstanceEntity getProcessInstanceByProcessInstanceId() {
        return processInstanceByProcessInstanceId;
    }

    public void setProcessInstanceByProcessInstanceId(ProcessInstanceEntity processInstanceByProcessInstanceId) {
        this.processInstanceByProcessInstanceId = processInstanceByProcessInstanceId;
    }

//    @ManyToOne
//    @JoinColumn(name = "execution_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
//    public ExecutionEntity getExecutionByExecutionId() {
//        return executionByExecutionId;
//    }
//
//    public void setExecutionByExecutionId(ExecutionEntity executionByExecutionId) {
//        this.executionByExecutionId = executionByExecutionId;
//    }

//    @ManyToOne
//    @JoinColumn(name = "activity_id", referencedColumnName = "activity_id", nullable = true, insertable = false, updatable = false)
//    public IncidentEntity getIncidentByActivityId() {
//        return incidentByActivityId;
//    }
//
//    public void setIncidentByActivityId(IncidentEntity incidentByActivityId) {
//        this.incidentByActivityId = incidentByActivityId;
//    }

//    @OneToMany(mappedBy = "activityInstanceByActivityInstanceId")
//    public Collection<IncidentEntity> getIncidentsById() {
//        return incidentsById;
//    }
//
//    public void setIncidentsById(Collection<IncidentEntity> incidentsById) {
//        this.incidentsById = incidentsById;
//    }

    @OneToMany(mappedBy = "activityInstanceByActivityInstanceId")
    public Collection<TaskEntity> getTasksById() {
        return tasksById;
    }

    public void setTasksById(Collection<TaskEntity> tasksById) {
        this.tasksById = tasksById;
    }



    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ActivityInstanceEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", parentActivityInstanceId='").append(parentActivityInstanceId).append('\'');
        sb.append(", activityType=").append(activityType);
        sb.append(", activityId='").append(activityId).append('\'');
        sb.append(", activityName='").append(activityName).append('\'');
        sb.append(", processInstanceId='").append(processInstanceId).append('\'');
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", removalTime=").append(removalTime);
        sb.append(", canceled=").append(canceled);
        sb.append(", executionId='").append(executionId).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
