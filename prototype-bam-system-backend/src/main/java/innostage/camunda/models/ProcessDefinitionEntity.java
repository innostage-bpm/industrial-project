package innostage.camunda.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "process_definition", schema = "public", catalog = "adaptor")
public class ProcessDefinitionEntity {
    private String id;
    private String key;
    private String name;
    private String deploymentId;
    private int version;
    @JsonIgnore
    private String schemaXml;
    @Transient
    private int numberOfIncidents;
    @Transient
    private int numberOfProcessInstances;
    @JsonIgnore
    private Collection<ProcessInstanceEntity> processInstancesById;

    public ProcessDefinitionEntity() {
    }

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "key", nullable = false, length = 255)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "deployment_id", nullable = false, length = 255)
    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    @Basic
    @Column(name = "version", nullable = false)
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Basic
    @Column(name = "schema_xml", columnDefinition="xml", nullable = true)
    @org.hibernate.annotations.Type(type= "innostage.camunda.models.sql_types.SQLXMLType")
    public String getSchemaXml() {
        return schemaXml;
    }

    public void setSchemaXml(String schemaXml) {
        this.schemaXml = schemaXml;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessDefinitionEntity that = (ProcessDefinitionEntity) o;
        return version == that.version &&
                Objects.equals(id, that.id) &&
                Objects.equals(key, that.key) &&
                Objects.equals(name, that.name) &&
                Objects.equals(deploymentId, that.deploymentId) &&
                Objects.equals(schemaXml, that.schemaXml);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key, name, deploymentId, version, schemaXml);
    }

    @OneToMany(mappedBy = "processDefinitionByDefinitionId")
    public Collection<ProcessInstanceEntity> getProcessInstancesById() {
        return processInstancesById;
    }

    public void setProcessInstancesById(Collection<ProcessInstanceEntity> processInstancesById) {
        this.processInstancesById = processInstancesById;
    }

    @Override
    public String toString() {
        return "ProcessDefinitionEntity{" +
                "id='" + id + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", deploymentId='" + deploymentId + '\'' +
                ", version=" + version +
                ", schemaXml='" + schemaXml + '\'' +
                ", processInstancesById=" + processInstancesById +
                '}';
    }

    @Transient
    public int getNumberOfIncidents() {
        int amountOfIncidents = 0;
        for (ProcessInstanceEntity processInstance : processInstancesById) {
            amountOfIncidents += processInstance.getIncidentsById().size();
        }
        return amountOfIncidents;
    }

    public void setNumberOfIncidents() {

    }

    @Transient
    public int getNumberOfProcessInstances() {
        return processInstancesById.size();
    }

    public void setNumberOfProcessInstances(int numberOfProcessInstances) {
        this.numberOfProcessInstances = numberOfProcessInstances;
    }
}
