package innostage.camunda.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import innostage.camunda.models.enums.ProcessInstanceState;
import innostage.camunda.models.sql_types.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "process_instance", schema = "public", catalog = "adaptor")
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class ProcessInstanceEntity {
    private String id;
    private String processDefinitionId;
    private String startUserId;
    private Timestamp startTime;
    private Timestamp endTime;
    private Timestamp removalTime;
//    private String startActivityId;
    private ProcessInstanceState state;
    private String rootProcessInstanceId;
    private Timestamp lastSyncTime;
    private Collection<ActivityInstanceEntity> activityInstancesById;
    private long durationInMillis;
    @JsonIgnore
    private Collection<ExecutionEntity> executionsById;
    private Collection<IncidentEntity> incidentsById;
    private ProcessDefinitionEntity processDefinitionByDefinitionId;
    private UserEntity userByStartUserId;
    @JsonIgnore
    private ProcessInstanceEntity processInstanceByRootProcessInstanceId;
    @JsonIgnore
    private Collection<ProcessInstanceEntity> processInstancesById;
    private Collection<VariableInstanceEntity> variablesById;

    public ProcessInstanceEntity() {
    }

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "definition_id", nullable = false, length = 255)
    public String getDefinitionId() {
        return processDefinitionId;
    }

    public void setDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    @Basic
    @Column(name = "start_user_id", nullable = true, length = 255)
    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "removal_time", nullable = true)
    public Timestamp getRemovalTime() {
        return removalTime;
    }

    public void setRemovalTime(Timestamp removalTime) {
        this.removalTime = removalTime;
    }

//    @Basic
//    @Column(name = "start_activity_id", nullable = false, length = 255)
//    public String getStartActivityId() {
//        return startActivityId;
//    }
//
//    public void setStartActivityId(String startActivityId) {
//        this.startActivityId = startActivityId;
//    }

    @Type( type = "pgsql_enum" )
    @Column(name = "state", nullable = false, columnDefinition = "process_instance_state")
    @Enumerated(EnumType.STRING)
    public ProcessInstanceState getStateId() {
        return state;
    }

    public void setStateId(ProcessInstanceState state) {
        this.state = state;
    }

    @Basic
    @Column(name = "root_process_instance_id", nullable = true, length = 255)
    public String getRootProcessInstanceId() {
        return rootProcessInstanceId;
    }

    public void setRootProcessInstanceId(String rootProcessInstanceId) {
        this.rootProcessInstanceId = rootProcessInstanceId;
    }

    @Basic
    @Column(name = "last_sync_time", nullable = false)
    public Timestamp getLastSyncTime() {
        return lastSyncTime;
    }

    public void setLastSyncTime(Timestamp lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessInstanceEntity that = (ProcessInstanceEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(processDefinitionId, that.processDefinitionId) &&
                Objects.equals(startUserId, that.startUserId) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(removalTime, that.removalTime) &&
//                Objects.equals(startActivityId, that.startActivityId) &&
                Objects.equals(state, that.state) &&
                Objects.equals(rootProcessInstanceId, that.rootProcessInstanceId) &&
                Objects.equals(lastSyncTime, that.lastSyncTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, processDefinitionId, startUserId, startTime, endTime, removalTime, /*startActivityId,*/ state, rootProcessInstanceId, lastSyncTime);
    }

    @OneToMany(mappedBy = "processInstanceByProcessInstanceId")
    public Collection<ActivityInstanceEntity> getActivityInstancesById() {
        return activityInstancesById;
    }

    public void setActivityInstancesById(Collection<ActivityInstanceEntity> activityInstancesById) {
        this.activityInstancesById = activityInstancesById;
    }

    @OneToMany(mappedBy = "processInstanceByProcessInstanceId")
    public Collection<ExecutionEntity> getExecutionsById() {
        return executionsById;
    }

    public void setExecutionsById(Collection<ExecutionEntity> executionsById) {
        this.executionsById = executionsById;
    }

    @OneToMany(mappedBy = "processInstanceByProcessInstanceId")
    public Collection<IncidentEntity> getIncidentsById() {
        return incidentsById;
    }

    public void setIncidentsById(Collection<IncidentEntity> incidentsById) {
        this.incidentsById = incidentsById;
    }

    @ManyToOne
    @JoinColumn(name = "definition_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ProcessDefinitionEntity getProcessDefinitionByDefinitionId() {
        return processDefinitionByDefinitionId;
    }

    public void setProcessDefinitionByDefinitionId(ProcessDefinitionEntity processDefinitionByDefinitionId) {
        this.processDefinitionByDefinitionId = processDefinitionByDefinitionId;
    }

    @ManyToOne
    @JoinColumn(name = "start_user_id", referencedColumnName = "id", insertable = false, updatable = false)
    public UserEntity getUserByStartUserId() {
        return userByStartUserId;
    }

    public void setUserByStartUserId(UserEntity userByStartUserId) {
        this.userByStartUserId = userByStartUserId;
    }

    @ManyToOne
    @JoinColumn(name = "root_process_instance_id", referencedColumnName = "id", insertable = false, updatable = false)
    public ProcessInstanceEntity getProcessInstanceByRootProcessInstanceId() {
        return processInstanceByRootProcessInstanceId;
    }

    public void setProcessInstanceByRootProcessInstanceId(ProcessInstanceEntity processInstanceByRootProcessInstanceId) {
        this.processInstanceByRootProcessInstanceId = processInstanceByRootProcessInstanceId;
    }

    @OneToMany(mappedBy = "processInstanceByRootProcessInstanceId")
    public Collection<ProcessInstanceEntity> getProcessInstancesById() {
        return processInstancesById;
    }

    public void setProcessInstancesById(Collection<ProcessInstanceEntity> processInstancesById) {
        this.processInstancesById = processInstancesById;
    }

    @OneToMany(mappedBy = "processInstanceByProcessInstanceId")
    public Collection<VariableInstanceEntity> getVariablesById() {
        return variablesById;
    }

    public void setVariablesById(Collection<VariableInstanceEntity> variablesById) {
        this.variablesById = variablesById;
    }

    @Basic
    @Column(name = "duration_in_millis", nullable = false)
    public long getDurationInMillis() {
        return durationInMillis;
    }

    public void setDurationInMillis(long durationInMillis) {
        this.durationInMillis = durationInMillis;
    }

}
