package innostage.camunda.models.enums;

public enum ProcessInstanceState {
    ACTIVE, SUSPENDED, COMPLETED, EXTERNALLY_TERMINATED, INTERNALLY_TERMINATED
}
