package innostage.camunda.models.enums;

public enum VariableType {

    Boolean, Bytes, Short, Integer, Long, Double, Date, String, Null, File, Object, Json, Xml

}
