package innostage.camunda.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "incident", schema = "public", catalog = "adaptor")
public class IncidentEntity {
    private String id;
    private String activityId;
    private Timestamp createTime;
    private String processInstanceId;
    private String incidentType;
    private String incidentMessage;
    private boolean open;
    private boolean deleted;
    private boolean resolved;
    @JsonIgnore
    private ProcessInstanceEntity processInstanceByProcessInstanceId;

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "activity_id", nullable = false, length = 255)
    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    @Basic
    @Column(name = "create_time", nullable = false)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "process_instance_id", nullable = false, length = 255)
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Basic
    @Column(name = "type", nullable = true, length = 255)
    public String getType() {
        return incidentType;
    }

    public void setType(String type) {
        this.incidentType = type;
    }

    @Basic
    @Column(name = "message", nullable = true, length = 255)
    public String getMessage() {
        return incidentMessage;
    }

    public void setMessage(String message) {
        this.incidentMessage = message;
    }

    @Basic
    @Column(name = "open", nullable = false)
    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "resolved", nullable = false)
    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IncidentEntity that = (IncidentEntity) o;
        return open == that.open &&
                deleted == that.deleted &&
                resolved == that.resolved &&
                Objects.equals(id, that.id) &&
                Objects.equals(activityId, that.activityId) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(processInstanceId, that.processInstanceId) &&
                Objects.equals(incidentType, that.incidentType) &&
                Objects.equals(incidentMessage, that.incidentMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, activityId, createTime, processInstanceId, incidentType, incidentMessage, open, deleted, resolved);
    }

    @ManyToOne
    @JoinColumn(name = "process_instance_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ProcessInstanceEntity getProcessInstanceByProcessInstanceId() {
        return processInstanceByProcessInstanceId;
    }

    public void setProcessInstanceByProcessInstanceId(ProcessInstanceEntity processInstanceByProcessInstanceId) {
        this.processInstanceByProcessInstanceId = processInstanceByProcessInstanceId;
    }


//    @ManyToOne
//    @JoinColumn(name = "activity_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
//    public ActivityInstanceEntity getActivityInstanceByActivityInstanceId() {
//        return activityInstanceByActivityInstanceId;
//    }
//
//    public void setActivityInstanceByActivityInstanceId(ActivityInstanceEntity activityInstanceByActivityInstanceId) {
//        this.activityInstanceByActivityInstanceId = activityInstanceByActivityInstanceId;
//    }

}
