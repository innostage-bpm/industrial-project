package innostage.camunda.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "task", schema = "public", catalog = "adaptor")
public class TaskEntity {
    private String id;
    private String parentTaskId;
    private String name;
    private String taskDefinitionKey;
    private String description;
    private Timestamp startTime;
    private Timestamp endTime;
    private String owner;
    private String assignee;
    private Timestamp removalTime;
    private Timestamp due;
    private Timestamp followUp;
    private String activityInstanceId;
    private int priority;
    private TaskEntity taskByParentTaskId;
    private Collection<TaskEntity> tasksById;
    private UserEntity userByOwner;
    private UserEntity userByAssignee;
    @JsonIgnore
    private ActivityInstanceEntity activityInstanceByActivityInstanceId;

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "parent_task_id", nullable = true, length = 255)
    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "task_definition_key", nullable = false, length = 255)
    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "owner", nullable = true, length = 255)
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "assignee", nullable = true, length = 255)
    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Basic
    @Column(name = "removal_time", nullable = true)
    public Timestamp getRemovalTime() {
        return removalTime;
    }

    public void setRemovalTime(Timestamp removalTime) {
        this.removalTime = removalTime;
    }

    @Basic
    @Column(name = "due_time", nullable = true)
    public Timestamp getDueTime() {
        return due;
    }

    public void setDueTime(Timestamp dueTime) {
        this.due = dueTime;
    }

    @Basic
    @Column(name = "followup_time", nullable = true)
    public Timestamp getFollowupTime() {
        return followUp;
    }

    public void setFollowupTime(Timestamp followupTime) {
        this.followUp = followupTime;
    }

    @Basic
    @Column(name = "activity_instance_id", nullable = true, length = 255)
    public String getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(String activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    @Basic
    @Column(name = "priority", nullable = false)
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskEntity that = (TaskEntity) o;
        return priority == that.priority &&
                Objects.equals(id, that.id) &&
                Objects.equals(parentTaskId, that.parentTaskId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(taskDefinitionKey, that.taskDefinitionKey) &&
                Objects.equals(description, that.description) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(owner, that.owner) &&
                Objects.equals(assignee, that.assignee) &&
                Objects.equals(removalTime, that.removalTime) &&
                Objects.equals(due, that.due) &&
                Objects.equals(followUp, that.followUp) &&
                Objects.equals(activityInstanceId, that.activityInstanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, parentTaskId, name, taskDefinitionKey, description, startTime, endTime, owner, assignee, removalTime, due, followUp, activityInstanceId, priority);
    }

    @ManyToOne
    @JoinColumn(name = "parent_task_id", referencedColumnName = "id", insertable = false, updatable = false)
    public TaskEntity getTaskByParentTaskId() {
        return taskByParentTaskId;
    }

    public void setTaskByParentTaskId(TaskEntity taskByParentTaskId) {
        this.taskByParentTaskId = taskByParentTaskId;
    }

    @OneToMany(mappedBy = "taskByParentTaskId")
    public Collection<TaskEntity> getTasksById() {
        return tasksById;
    }

    public void setTasksById(Collection<TaskEntity> tasksById) {
        this.tasksById = tasksById;
    }

    @ManyToOne
    @JoinColumn(name = "owner", referencedColumnName = "id", insertable = false, updatable = false)
    public UserEntity getUserByOwner() {
        return userByOwner;
    }

    public void setUserByOwner(UserEntity userByOwner) {
        this.userByOwner = userByOwner;
    }

    @ManyToOne
    @JoinColumn(name = "assignee", referencedColumnName = "id", insertable = false, updatable = false)
    public UserEntity getUserByAssignee() {
        return userByAssignee;
    }

    public void setUserByAssignee(UserEntity userByAssignee) {
        this.userByAssignee = userByAssignee;
    }

    @ManyToOne
    @JoinColumn(name = "activity_instance_id", referencedColumnName = "id", nullable = true, insertable = false, updatable = false)
    public ActivityInstanceEntity getActivityInstanceByActivityInstanceId() {
        return activityInstanceByActivityInstanceId;
    }

    public void setActivityInstanceByActivityInstanceId(ActivityInstanceEntity activityInstanceByActivityInstanceId) {
        this.activityInstanceByActivityInstanceId = activityInstanceByActivityInstanceId;
    }
}
