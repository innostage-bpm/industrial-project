package innostage.camunda.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "public", catalog = "adaptor")
public class UserEntity {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    @JsonIgnore private Collection<ProcessInstanceEntity> processInstancesById;
    @JsonIgnore private Collection<TaskEntity> tasksById;
    @JsonIgnore private Collection<TaskEntity> tasksById_0;

    public UserEntity(String id, String firstName, String lastName, String email, Collection<ProcessInstanceEntity> processInstancesById, Collection<TaskEntity> tasksById, Collection<TaskEntity> tasksById_0) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.processInstancesById = processInstancesById;
        this.tasksById = tasksById;
        this.tasksById_0 = tasksById_0;
    }

    public UserEntity() {
    }

    @Id
    @Column(name = "id", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 255)
    public String getFirstname() {
        return firstName;
    }

    public void setFirstname(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 255)
    public String getLastname() {
        return lastName;
    }

    public void setLastname(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email);
    }

    @OneToMany(mappedBy = "userByStartUserId")
    public Collection<ProcessInstanceEntity> getProcessInstancesById() {
        return processInstancesById;
    }

    public void setProcessInstancesById(Collection<ProcessInstanceEntity> processInstancesById) {
        this.processInstancesById = processInstancesById;
    }

    @OneToMany(mappedBy = "userByOwner")
    public Collection<TaskEntity> getTasksById() {
        return tasksById;
    }

    public void setTasksById(Collection<TaskEntity> tasksById) {
        this.tasksById = tasksById;
    }

    @OneToMany(mappedBy = "userByAssignee")
    public Collection<TaskEntity> getTasksById_0() {
        return tasksById_0;
    }

    public void setTasksById_0(Collection<TaskEntity> tasksById_0) {
        this.tasksById_0 = tasksById_0;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", processInstancesById=" + processInstancesById +
                ", tasksById=" + tasksById +
                ", tasksById_0=" + tasksById_0 +
                '}';
    }
}
