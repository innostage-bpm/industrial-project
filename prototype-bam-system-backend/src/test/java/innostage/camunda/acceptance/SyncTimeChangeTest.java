package acceptance;

import innostage.camunda.utils.PropertiesHandler;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@RunWith(SpringRunner.class)
public class SyncTimeChangeTest {

    static PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    public static WebDriver driver;
    private static final String BASE_URL = PROPERTIES_HANDLER.getProperty("frontend.url");

    @BeforeClass
    public static void setup() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("org.apache.http");
        root.setLevel(ch.qos.logback.classic.Level.INFO);
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
        System.setProperty("webdriver.chrome.silentOutput", "true");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--log-level=3");
        options.addArguments("--silent");
        driver = new ChromeDriver(options);
        driver.get(BASE_URL);
    }

    @Test
    public void whenEnterInvalidSyncTimeShouldNotBeSaved() {
        //given
        WebElement inputTime = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[1]/div/div/div[8]/div/div[2]/div/input"));
        String actualSyncTimeBefore = inputTime.getAttribute("value");
        //when
        String invalidSyncTime = "ten";
        WebElement updateBtn = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[1]/div/div/div[8]/div/button[2]/span[1]"));
        inputTime.clear();
        inputTime.sendKeys(invalidSyncTime);
        updateBtn.click();
        driver.navigate().refresh();
        WebDriverWait wait=new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.attributeToBe(By.xpath("//*[@id=\"app\"]/div/div[1]/div/div/div[8]/div/div[2]/div/input"),"value",actualSyncTimeBefore));
        inputTime = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[1]/div/div/div[8]/div/div[2]/div/input"));
        String actualSyncTimeAfter = inputTime.getAttribute("value");
        //then
        Assert.assertEquals(actualSyncTimeBefore, actualSyncTimeAfter);
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }
}