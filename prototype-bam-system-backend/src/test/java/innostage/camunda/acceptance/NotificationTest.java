package acceptance;

import innostage.camunda.services.impl.IncidentServiceImpl;
import innostage.camunda.utils.PropertiesHandler;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.logging.Level;

@SpringBootTest(classes = IncidentServiceImpl.class)
@ComponentScan("innostage.camunda")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@RunWith(SpringRunner.class)
public class NotificationTest {

    private static final long FRONTEND_INC_UPDATE_PERIOD = 3200000;
    private static final PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    public static WebDriver driver;
    private static final String BASE_URL = PROPERTIES_HANDLER.getProperty("frontend.url");

    @Autowired
    IncidentServiceImpl incidentService;

    @BeforeClass
    public static void setup() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("org.apache.http");
        root.setLevel(ch.qos.logback.classic.Level.INFO);
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
        System.setProperty("webdriver.chrome.silentOutput", "true");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--log-level=3");
        options.addArguments("--silent");
        driver = new ChromeDriver(options);
        driver.get(BASE_URL);
    }

    @Test
    public void notificationsShouldDisplayedOrNotAccordingToTheirExistence() {
        String notificationLocator = "//*[@id=\"app\"]/div[2]";
        if (!incidentService.getIncidentUpdates(FRONTEND_INC_UPDATE_PERIOD).isEmpty())  {
            //wait for 10 seconds to check
            WebDriverWait wait=new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(notificationLocator)));
            Assert.assertTrue(true); //driver.findElement(By.xpath(notificationLocator)).isDisplayed());
        } else {
            Assert.assertTrue("There no incidents for notification period in the DB",
                    verifyElementAbsent(notificationLocator));
        }
    }

    private boolean verifyElementAbsent(String locator) {
        try {
            driver.findElement(By.xpath(locator));
            return false;
        } catch (NoSuchElementException e) {
            return true;
        }
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

}
