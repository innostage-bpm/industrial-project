package acceptance;

import innostage.camunda.utils.PropertiesHandler;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@RunWith(SpringRunner.class)
public class InitPageTest {

    static PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    public static WebDriver driver;
    private static final String BASE_URL = PROPERTIES_HANDLER.getProperty("frontend.url");

    @BeforeClass
    public static void setup() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("org.apache.http");
        root.setLevel(ch.qos.logback.classic.Level.INFO);
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
        System.setProperty("webdriver.chrome.silentOutput", "true");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--log-level=3");
        options.addArguments("--silent");
        driver = new ChromeDriver(options);
        driver.get(BASE_URL);
    }

    @Test
    public void dashboardShouldBePresent() {
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div")).isDisplayed());
    }

    @Test
    public void dataTableShouldBePresent() {
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[4]")).isDisplayed());
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }


}

