package acceptance;

import innostage.camunda.utils.PropertiesHandler;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@RunWith(SpringRunner.class)
public class BPMNSchemasTest {

    static PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    public static WebDriver driver;
    private static final String BASE_URL = PROPERTIES_HANDLER.getProperty("frontend.url");
    private static final String DEFINITIONS_PAGE = BASE_URL + "/definitions";

    @BeforeClass
    public static void setup() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("org.apache.http");
        root.setLevel(ch.qos.logback.classic.Level.INFO);
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
        System.setProperty("webdriver.chrome.silentOutput", "true");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--window-size=1920,2000");
        options.addArguments("--log-level=3");
        options.addArguments("--silent");
        driver = new ChromeDriver(options);
    }

    @Test
    public void whenClickOnShowSchemaShouldDisplay() {
        //given
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(DEFINITIONS_PAGE);
        //when
        driver.findElement(By.xpath("/html/body/div[1]/div/div[4]/div[2]/div/div/div/table/tbody/tr[2]/td[7]/button/span[1]")).click();
        //then
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"app\"]/div/*/div/div/div/div")).isDisplayed());
    }


    @AfterClass
    public static void tearDown() {
        driver.close();
    }

}
