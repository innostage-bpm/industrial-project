package acceptance;

import innostage.camunda.utils.PropertiesHandler;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class ProcessDurationTest {

    static PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
    public static WebDriver driver;
    private static final String BASE_URL = PROPERTIES_HANDLER.getProperty("frontend.url");
    private static final String INSTANCES_PAGE = BASE_URL + "/instances";
    private static final String ACTIVITIES_PAGE = BASE_URL + "/activities";

    @BeforeClass
    public static void setup() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("org.apache.http");
        root.setLevel(ch.qos.logback.classic.Level.INFO);
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
        System.setProperty("webdriver.chrome.silentOutput", "true");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--log-level=3");
        options.addArguments("--silent");
        driver = new ChromeDriver(options);
        driver.get(BASE_URL);
    }

    @Test
    public void durationForProcessInstancesShouldBePresent() {
        //when
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(INSTANCES_PAGE);
        //then
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div[2]/div/div/div/table/thead/tr/th[7]/span")).isDisplayed());
    }

    @Test
    public void durationForProcessActivitiesShouldBePresent() {
        //when
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(ACTIVITIES_PAGE);
        //then
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div[2]/div/div/div/table/thead/tr/th[7]/span")).isDisplayed());
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }
}
