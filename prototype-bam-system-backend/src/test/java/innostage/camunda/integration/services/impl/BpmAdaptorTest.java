package integration.services.impl;


import innostage.camunda.services.BpmAdaptor;
import innostage.camunda.services.impl.BpmAdaptorImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BpmAdaptorImpl.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BpmAdaptorTest {

    @Autowired
    BpmAdaptor bpmAdaptor;

    @Test
    public void whenRequestedShouldReturnData() throws Exception {
        //when
        String response = bpmAdaptor.getProcessDefinitions();
        //then
        Assert.assertTrue(response != null && !response.isEmpty());
    }
}
