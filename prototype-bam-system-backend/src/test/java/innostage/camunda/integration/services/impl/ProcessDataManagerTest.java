package integration.services.impl;

import innostage.camunda.models.ProcessInstanceEntity;
import innostage.camunda.services.ProcessDataManager;
import innostage.camunda.services.impl.ProcessInstanceServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {
        ProcessDataManager.class,
        ProcessInstanceServiceImpl.class})
@ComponentScan("innostage.camunda")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ProcessDataManagerTest {

    @Autowired
    ProcessDataManager processDataManager;
    @Autowired
    ProcessInstanceServiceImpl processInstanceServiceImpl;

    private static final double FROM_NANOS_TO_SECONDS = 1000000000.0;
    private static double syncDuration;

    @Before
    public void beforeMethod() {
        long startTime = System.nanoTime();
        processDataManager.syncData();
        long endTime = System.nanoTime();
        syncDuration = (endTime - startTime) / FROM_NANOS_TO_SECONDS;
        org.junit.Assume.assumeTrue((processInstanceServiceImpl.findAll().size() > 0));
    }

    @Test
    public void processShouldBeUpdated() throws InterruptedException {
            //given
            ProcessInstanceEntity processInstanceEntity = processInstanceServiceImpl.findAll().get(0);
            String processId = processInstanceEntity.getId();
            Timestamp processLastSyncTime = processInstanceEntity.getLastSyncTime();
            //when
            processDataManager.updateProcessInstanceData(processId);
            Thread.sleep(Math.round(syncDuration) * 1000);
            //then
            processInstanceEntity = processInstanceServiceImpl.getByID(processId);
            Assert.assertTrue(processInstanceEntity.getLastSyncTime().after(processLastSyncTime));
    }

}
