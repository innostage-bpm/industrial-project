package integration.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import innostage.camunda.models.ProcessInstanceEntity;
import innostage.camunda.services.BpmAdaptor;
import innostage.camunda.services.MetricAggregator;
import innostage.camunda.services.ProcessDataManager;
import innostage.camunda.services.impl.*;
import innostage.camunda.services.models.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Logger;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =
        {ProcessDefinitionServiceImpl.class,
                ActivityInstanceServiceImpl.class,
                TaskServiceImpl.class,
                IncidentServiceImpl.class,
                UserServiceImpl.class,
                VariableInstanceServiceImpl.class,
                ProcessInstanceServiceImpl.class,
                ProcessDataManager.class,
                VariableHistoryServiceImpl.class
        }
)
@ComponentScan("innostage.camunda")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AggregatorTest {

    @Autowired
    private ProcessDefinitionService processDefinitionService;
    @Autowired
    private ActivityInstanceService activityInstanceService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private IncidentService incidentService;
    @Autowired
    private UserService userService;
    @Autowired
    private VariableInstanceService variableInstanceService;
    @Autowired
    private VariableHistoryService variableHistoryService;
    @Autowired
    private ProcessInstanceService processInstanceService;
    @Autowired
    ProcessDataManager processDataManager;
    private final Logger LOGGER = Logger.getLogger(AggregatorTest.class.getName());
    private static final double FROM_NANOS_TO_SECONDS = 1000000000.0;
    private static double syncDuration;

    @Before
    public void beforeMethod() {
        long startTime = System.nanoTime();
        processDataManager.syncData();
        long endTime = System.nanoTime();
        syncDuration = (endTime - startTime) / FROM_NANOS_TO_SECONDS;
        org.junit.Assume.assumeTrue((processInstanceService.findAll().size() > 0));
    }
    @Test
    public void singleProcessSyncTest() throws InterruptedException {
        //given
        String processInstanceId = getFirstProcessInstanceIdFromEngine();
        Timestamp syncTime = processInstanceService.getByID(processInstanceId).getLastSyncTime();
        //when
        MetricAggregator aggregator = new MetricAggregator(processDefinitionService,
                activityInstanceService,
                taskService,
                userService,
                incidentService, variableHistoryService, variableInstanceService, processInstanceService);
        aggregator.updateData(processInstanceId);
        Thread.sleep(Math.round(syncDuration) * 1000);
        //then
        Assert.assertTrue(processInstanceService.getByID(processInstanceId).getLastSyncTime().after(syncTime));
    }

    @Test
    public void fullSyncTest() {
        MetricAggregator aggregator = new MetricAggregator(processDefinitionService,
                activityInstanceService,
                taskService,
                userService,
                incidentService, variableHistoryService, variableInstanceService, processInstanceService);
        aggregator.updateData();
    }

    private String getFirstProcessInstanceIdFromEngine() {
        BpmAdaptor adaptor = new BpmAdaptorImpl();
        Type processInstanceType = new TypeToken<List<ProcessInstanceEntity>>() {
        }.getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        List<ProcessInstanceEntity> processInstances = null;
        try {
            processInstances = gson.fromJson(adaptor.getProcessInstances(), processInstanceType);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.severe("An error during requesting process instances from BPMN engine");
        }
        if (processInstances != null && !processInstances.isEmpty()) {
            return processInstances.get(0).getId();
        }
        return null;
    }
}

