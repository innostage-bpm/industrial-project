package integration.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import innostage.camunda.models.VariableHistoryEntity;
import innostage.camunda.models.VariableInstanceEntity;
import innostage.camunda.services.MetricAggregator;
import innostage.camunda.services.impl.VariableInstanceServiceImpl;
import innostage.camunda.services.models.*;
import innostage.camunda.utils.VariableInstanceDeserializer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VariableInstanceServiceImpl.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ComponentScan("innostage.camunda")
public class VariableInstanceDeserializerTest {

    @Mock
    private ProcessDefinitionService processDefinitionService;
    @Mock
    private ActivityInstanceService activityInstanceService;
    @Mock
    private TaskService taskService;
    @Mock
    private IncidentService incidentService;
    @Mock
    private UserService userService;
    @Autowired
    private VariableInstanceService variableInstanceService;
    @Mock
    private VariableHistoryService variableHistoryService;
    @Mock
    private ProcessInstanceService processInstanceService;

    private final MetricAggregator aggregator = new MetricAggregator(
            processDefinitionService,
            activityInstanceService,
            taskService,
            userService,
            incidentService,
            variableHistoryService,
            variableInstanceService,
            processInstanceService);

    private final Type variableInstanceEntity = new TypeToken<ArrayList<VariableInstanceEntity>>(){}.getType();
    private final GsonBuilder gsonBuilder = new GsonBuilder();

    @Test
    public void integrationTest() throws Exception {
        //Possible to reduce time of test with passing request param
        HashMap<String, String> data = aggregator.requestData();
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableInstanceDeserializer());
        Gson gson = gsonBuilder.create();
        List<VariableInstanceEntity> variable_instances = gson.fromJson(data.get("variable_instances"),variableInstanceEntity);
        if (!data.get("variable_instances").isEmpty()) {
            Assert.assertFalse(variable_instances.isEmpty());
        }
    }
}
