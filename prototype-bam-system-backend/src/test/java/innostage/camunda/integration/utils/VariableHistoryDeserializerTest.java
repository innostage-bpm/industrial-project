package integration.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import innostage.camunda.models.VariableHistoryEntity;
import innostage.camunda.services.MetricAggregator;
import innostage.camunda.services.impl.VariableHistoryServiceImpl;
import innostage.camunda.services.models.*;
import innostage.camunda.utils.VariableHistoryDeserializer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VariableHistoryServiceImpl.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ComponentScan("innostage.camunda")
public class VariableHistoryDeserializerTest {

    @Mock
    private ProcessDefinitionService processDefinitionService;
    @Mock
    ActivityInstanceService activityInstanceService;
    @Mock
    TaskService taskService;
    @Mock
    IncidentService incidentService;
    @Mock
    UserService userService;
    @Mock
    VariableInstanceService variableInstanceService;
    @Autowired
    VariableHistoryService variableHistoryService;
    @Mock
    ProcessInstanceService processInstanceService;

    private final MetricAggregator aggregator = new MetricAggregator(processDefinitionService,
            activityInstanceService,
            taskService,
            userService,
            incidentService,
            variableHistoryService,
            variableInstanceService,
            processInstanceService);

    private final GsonBuilder gsonBuilder = new GsonBuilder();
    private final Type variableHistoryEntity = new TypeToken<ArrayList<VariableHistoryEntity>>() {
    }.getType();
    private HashMap<String, String> data = new HashMap<>();

    @Test
    public void integrationTest() throws Exception {
        //Possible to reduce time of test with passing request param
        HashMap<String, String> data = aggregator.requestData();
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableHistoryDeserializer());
        Gson gson = gsonBuilder.create();
        List<VariableHistoryEntity> variable_details = gson.fromJson(data.get("variable_details"), variableHistoryEntity);
        if (!data.get("variable_details").isEmpty()) {
            Assert.assertFalse(variable_details.isEmpty());
        }
    }
}
