package unit.utils;

import innostage.camunda.models.ProcessDefinitionEntity;
import innostage.camunda.services.impl.ProcessDefinitionServiceImpl;
import innostage.camunda.services.models.ProcessDefinitionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProcessDefinitionServiceImpl.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ComponentScan("innostage.camunda")
public class SpringDataTest {

    @Autowired
    private ProcessDefinitionService processDefinitionService;

    @Test
    public void whenSavedThenShouldBeInDB() {
        // given
        ProcessDefinitionEntity processDefinitionEntity = new ProcessDefinitionEntity();
        processDefinitionEntity.setId("id2");
        processDefinitionEntity.setKey("somekey");
        processDefinitionEntity.setName("somename");
        processDefinitionEntity.setDeploymentId("3");
        processDefinitionEntity.setVersion(2);
        processDefinitionEntity.setSchemaXml("sdgdsg");
        processDefinitionService.save(processDefinitionEntity);

        // when
        ProcessDefinitionEntity found = processDefinitionService.getByID(processDefinitionEntity.getId());

        // then
        Assert.assertEquals(found.getId(), processDefinitionEntity.getId());
    }

}