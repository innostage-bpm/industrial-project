package unit.utils;

import innostage.camunda.models.ProcessDefinitionEntity;
import innostage.camunda.models.ProcessInstanceEntity;
import innostage.camunda.models.UserEntity;
import innostage.camunda.models.enums.ProcessInstanceState;
import innostage.camunda.repository.ProcessDefinitionRepository;
import innostage.camunda.repository.ProcessInstanceRepository;
import innostage.camunda.repository.UserRepository;
import innostage.camunda.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        ProcessDefinitionRepository.class,
        UserRepository.class,
        ProcessInstanceRepository.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ComponentScan("innostage.camunda")
public class HibernateComplexTest {

    @Autowired
    private ProcessDefinitionRepository processDefinitionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProcessInstanceRepository processInstanceRepository;

    @Test
    public void whenSavedShouldBeInTheDataBaseComplex() {

        //given
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstname("firstname");
        userEntity.setLastname("lastname");
        userEntity.setEmail("mail");
        userEntity.setId("1");
        ProcessInstanceEntity processInstanceEntity = new ProcessInstanceEntity();
        processInstanceEntity.setDefinitionId("id3");
        processInstanceEntity.setId("someid");
        processInstanceEntity.setStartUserId("1");
        processInstanceEntity.setStateId(ProcessInstanceState.ACTIVE);
        processInstanceEntity.setStartTime(new Timestamp(System.currentTimeMillis()));
        processInstanceEntity.setLastSyncTime(new Timestamp(System.currentTimeMillis()));
        session.beginTransaction();
        ProcessDefinitionEntity processDefinitionEntity = new ProcessDefinitionEntity();
        processDefinitionEntity.setId("id3");
        processDefinitionEntity.setKey("somekey");
        processDefinitionEntity.setName("somename");
        processDefinitionEntity.setDeploymentId("3");
        processDefinitionEntity.setVersion(2);
        processDefinitionEntity.setSchemaXml("sdgdsg");

        //when

        session.save(userEntity);
        session.save(processDefinitionEntity);
        session.save(processInstanceEntity);
        session.getTransaction().commit();
        session.close();

        //then

        Assert.assertTrue(processDefinitionRepository.count() > 0 &&
                processInstanceRepository.count() > 0 &&
                userRepository.count() > 0
        );
    }

    @After
    public void cleanUpDB() {
        processInstanceRepository.deleteAll();
        processDefinitionRepository.deleteAll();
    }
}
