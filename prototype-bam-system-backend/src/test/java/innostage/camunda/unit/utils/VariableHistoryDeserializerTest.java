package unit.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import innostage.camunda.models.VariableHistoryEntity;
import innostage.camunda.utils.VariableHistoryDeserializer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VariableHistoryDeserializer.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ComponentScan("innostage.camunda")
public class VariableHistoryDeserializerTest {

    private final GsonBuilder gsonBuilder = new GsonBuilder();
    private final Type variableHistoryEntity = new TypeToken<ArrayList<VariableHistoryEntity>>() {
    }.getType();
    private final HashMap<String, String> data = new HashMap<>();

    @Test
    public void basisPath_01_test() {
        data.put("variable_details", INVALID_TYPE_JSON);
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableHistoryDeserializer());
        Gson gson = gsonBuilder.create();
        List<VariableHistoryEntity> variable_details = gson.fromJson(data.get("variable_details"), variableHistoryEntity);
        Assert.assertNull(variable_details.get(0));
    }

    @Test
    public void basisPath_02_test() {
        data.put("variable_details", CASE_DEFINITION_JSON);
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableHistoryDeserializer());
        Gson gson = gsonBuilder.create();
        List<VariableHistoryEntity> variable_details = gson.fromJson(data.get("variable_details"), variableHistoryEntity);
        Assert.assertNull(variable_details.get(0));
    }

    @Test
    public void basisPath_03_test() {
        data.put("variable_details", NOT_CASE_DEF_VALUE_TASK_ID_NOT_NULL);
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableHistoryDeserializer());
        Gson gson = gsonBuilder.create();
        List<VariableHistoryEntity> variable_details = gson.fromJson(data.get("variable_details"), variableHistoryEntity);
        Assert.assertNotNull(variable_details.get(0));
    }

    @Test
    public void basisPath_04_test() {
        data.put("variable_details", NOT_CASE_DEF_VALUE_NOT_NULL_TASK_ID_NULL);
        gsonBuilder.registerTypeAdapter(VariableHistoryEntity.class, new VariableHistoryDeserializer());
        Gson gson = gsonBuilder.create();
        List<VariableHistoryEntity> variable_details = gson.fromJson(data.get("variable_details"), variableHistoryEntity);
        Assert.assertNotNull(variable_details.get(0));
    }

    //JSON examples
    private final static String INVALID_TYPE_JSON = "[ {\n" +
            "        \"type\": \"InvalidType\",\n" +
            "        \"id\": \"b373d1bc-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"processDefinitionKey\": null,\n" +
            "        \"processDefinitionId\": null,\n" +
            "        \"processInstanceId\": null,\n" +
            "        \"activityInstanceId\": \"b373aaaa-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"executionId\": null,\n" +
            "        \"caseDefinitionKey\": \"ReviewInvoiceCase\",\n" +
            "        \"caseDefinitionId\": \"ReviewInvoiceCase:2:b29e31bc-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"caseInstanceId\": \"b373aaaa-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"caseExecutionId\": \"b373aaaa-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"taskId\": null,\n" +
            "        \"tenantId\": null,\n" +
            "        \"userOperationId\": \"b3730e64-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"time\": \"2020-03-17T15:06:15.034+0300\",\n" +
            "        \"removalTime\": null,\n" +
            "        \"rootProcessInstanceId\": null,\n" +
            "        \"variableName\": \"amount\",\n" +
            "        \"variableInstanceId\": \"b373d1bb-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"variableType\": \"Double\",\n" +
            "        \"value\": 10.99,\n" +
            "        \"valueInfo\": {},\n" +
            "        \"revision\": 0,\n" +
            "        \"errorMessage\": null\n" +
            "    } ]";

    private final static String CASE_DEFINITION_JSON = "[ {\n" +
            "        \"type\": \"variableUpdate\",\n" +
            "        \"id\": \"0896b84e-8155-11ea-ab75-164f8a355b88\",\n" +
            "        \"processDefinitionKey\": null,\n" +
            "        \"processDefinitionId\": null,\n" +
            "        \"processInstanceId\": null,\n" +
            "        \"activityInstanceId\": \"b373f8d8-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"executionId\": null,\n" +
            "        \"caseDefinitionKey\": \"ReviewInvoiceCase\",\n" +
            "        \"caseDefinitionId\": \"ReviewInvoiceCase:2:b29e31bc-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"caseInstanceId\": \"b373aaaa-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"caseExecutionId\": \"b373aaaa-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"taskId\": null,\n" +
            "        \"tenantId\": null,\n" +
            "        \"userOperationId\": \"0896b84d-8155-11ea-ab75-164f8a355b88\",\n" +
            "        \"time\": \"2020-04-18T12:14:40.452+0300\",\n" +
            "        \"removalTime\": null,\n" +
            "        \"rootProcessInstanceId\": null,\n" +
            "        \"variableName\": \"reviewer\",\n" +
            "        \"variableInstanceId\": \"0896b84c-8155-11ea-ab75-164f8a355b88\",\n" +
            "        \"variableType\": \"String\",\n" +
            "        \"value\": \"demo\",\n" +
            "        \"valueInfo\": {},\n" +
            "        \"revision\": 0,\n" +
            "        \"errorMessage\": null\n" +
            "    } ]";

    private final static String NOT_CASE_DEF_VALUE_TASK_ID_NOT_NULL = "[ {\n" +
            "        \"type\": \"variableUpdate\",\n" +
            "        \"id\": \"b35dff57-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"processDefinitionKey\": \"invoice\",\n" +
            "        \"processDefinitionId\": \"invoice:2:b29b247a-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"processInstanceId\": \"b35dd83c-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"activityInstanceId\": \"StartEvent_1:b35dff53-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"executionId\": \"b35dd83c-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"caseDefinitionKey\": null,\n" +
            "        \"caseDefinitionId\": null,\n" +
            "        \"caseInstanceId\": null,\n" +
            "        \"caseExecutionId\": null,\n" +
            "        \"taskId\": some_id,\n" +
            "        \"tenantId\": null,\n" +
            "        \"userOperationId\": null,\n" +
            "        \"time\": \"2020-03-03T15:06:14.945+0300\",\n" +
            "        \"removalTime\": null,\n" +
            "        \"rootProcessInstanceId\": \"b35dd83c-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"variableName\": \"invoiceDocument\",\n" +
            "        \"variableInstanceId\": \"b35dff4f-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"variableType\": \"File\",\n" +
            "        \"value\": null,\n" +
            "        \"valueInfo\": {\n" +
            "            \"filename\": \"invoice.pdf\",\n" +
            "            \"mimeType\": \"application/pdf\"\n" +
            "        },\n" +
            "        \"revision\": 0,\n" +
            "        \"errorMessage\": null\n" +
            "    } ]";

    private final static String NOT_CASE_DEF_VALUE_NOT_NULL_TASK_ID_NULL = "[ {\n" +
            "        \"type\": \"variableUpdate\",\n" +
            "        \"id\": \"b35dff59-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"processDefinitionKey\": \"invoice\",\n" +
            "        \"processDefinitionId\": \"invoice:2:b29b247a-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"processInstanceId\": \"b35dd83c-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"activityInstanceId\": \"StartEvent_1:b35dff53-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"executionId\": \"b35dd83c-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"caseDefinitionKey\": null,\n" +
            "        \"caseDefinitionId\": null,\n" +
            "        \"caseInstanceId\": null,\n" +
            "        \"caseExecutionId\": null,\n" +
            "        \"taskId\": null,\n" +
            "        \"tenantId\": null,\n" +
            "        \"userOperationId\": null,\n" +
            "        \"time\": \"2020-03-03T15:06:14.945+0300\",\n" +
            "        \"removalTime\": null,\n" +
            "        \"rootProcessInstanceId\": \"b35dd83c-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"variableName\": \"creditor\",\n" +
            "        \"variableInstanceId\": \"b35dff51-6847-11ea-a5e5-164f8a355b88\",\n" +
            "        \"variableType\": \"String\",\n" +
            "        \"value\": \"Bobby's Office Supplies\",\n" +
            "        \"valueInfo\": {},\n" +
            "        \"revision\": 0,\n" +
            "        \"errorMessage\": null\n" +
            "    } ]";

}
