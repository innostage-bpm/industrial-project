package unit.utils;

import innostage.camunda.models.ProcessDefinitionEntity;
import innostage.camunda.repository.ProcessDefinitionRepository;
import innostage.camunda.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProcessDefinitionRepository.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ComponentScan("innostage.camunda")
public class HibernateTest {

    @Autowired
    private ProcessDefinitionRepository processDefinitionRepository;

    @Test
    public void whenSavedShouldBeInTheDataBaseSimple() {
        //given
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        ProcessDefinitionEntity processDefinitionEntity = new ProcessDefinitionEntity();
        processDefinitionEntity.setId("id4");
        processDefinitionEntity.setKey("somekey2");
        processDefinitionEntity.setName("somename2");
        processDefinitionEntity.setDeploymentId("4");
        processDefinitionEntity.setVersion(4);
        processDefinitionEntity.setSchemaXml("sdgdsg2");

        //when
        session.save(processDefinitionEntity);
        session.getTransaction().commit();
        session.close();

        //then
        ProcessDefinitionEntity found =
                processDefinitionRepository.findById(processDefinitionEntity.getId()).get();
        Assert.assertEquals(found.getId(), processDefinitionEntity.getId());
    }

    @After
    public void cleanUpDB() {
        processDefinitionRepository.deleteAll();
    }
}
