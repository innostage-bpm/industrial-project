package unit.utils;

import innostage.camunda.services.exceptions.EngineAddressIsInvalidException;
import innostage.camunda.utils.PropertiesHandler;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(JUnitParamsRunner.class)
@ComponentScan("innostage.camunda")
public class PropertiesHandlerTest {

	static PropertiesHandler PROPERTIES_HANDLER = new PropertiesHandler();
	private static final String INITIAL_ENGINE_ADDRESS = PROPERTIES_HANDLER.getBpmnEngineAddress();

	// Generated data: https://pairwise.teremokgames.com/r2ow/

	@Test
	@Parameters({
			"ftp://my.website.dot.io:8080 new--end-point",
			"ftp://1.1.1.1.ru:55555/endpoint",
			"ftp://0.0.0.0.com:8/end-point",
			"ftp://192.168.10.1.io:8 new-end-point",
			"ftp://www.website.ru:80/new--end-point",
			"ftp://localhost.com:8080  ",
			"https://website.ru:80 endpoint",
			"scp://my.website.ru:8 end-point",
			"scp://my.website.dot.com:80/new-end-point",
			"scp://1.1.1.1.io:8080 new--end-point",
			"scp://0.0.0.0.ru:55555/endpoint",
			"scp://192.168.10.1.com:8 end-point",
			"scp://www.website.io:80/new-end-point",
			"scp://localhost.ru:8080/new--end-point",
			"scp://website.com:55555  ",
			"scp://my-website.io:8/endpoint",
			"http://1.1.1.1.ru:8/new--end-point",
			"http://0.0.0.0.com:80/ ",
			"http://192.168.10.1.io:8080 endpoint",
			"http://localhost.com:8 new-end-point",
			"http://website.io:8/new--end-point",
			"http://my-website.ru:80 endpoint",
			"ftp://1.1.1.1.com:8080/new-end-point",
			"ftp://0.0.0.0.io:55555 new--end-point",
			"ftp://192.168.10.1.ru:8/endpoint",
			"ftp://www.website.com:8 end-point",
			"ftp://localhost.io:80/new-end-point",
			"ftp://website.ru:8080 new--end-point",
			"ftp://my-website.com:55555/ ",
			"ftp://my.website.dot.ru:80 end-point",
			"https://0.0.0.0.ru:8/new--end-point",
			"https://website.com:8 new-end-point",
			"https://my-website.io:80/new--end-point",
			"https://my-website.io:80/new--end-point",
			"https://my.website.ru:8080 endpoint",
			"scp://192.168.10.1.io:55555 new--end-point",
			"scp://www.website.ru:8/endpoint",
			"scp://localhost.com:80 end-point",
			"scp://website.io:8080/new-end-point",
			"scp://my-website.ru:55555 new--end-point",
			"scp://my.website.com:8/ ",
			"scp://my.website.dot.io:8 endpoint",
			"scp://1.1.1.1.ru:80/end-point",
			"scp://0.0.0.0.com:8080/new-end-point",
			"http://website.ru:8 end-point",
			"http://my.website.io:80/new--end-point",
			"http://my.website.dot.ru:8080 endpoint",
			"https://1.1.1.1.com:8/ ",
			"http://0.0.0.0.io:8 new-end-point",
			"http://192.168.10.1.ru:80/new--end-point",
	})
	public void whenInputtedInvalidEngineAddress__thenThrowException(
			String engineAddress) {
		assertThrows(EngineAddressIsInvalidException.class, () -> {
			PROPERTIES_HANDLER.setBpmnEngineAddress(engineAddress);
		});
	}

	@Test
	@Parameters({
			"https://my-website.com:8080/end-point",
			"https://my.website.io:55555/new-end-point",
			"http://www.website.ru:55555/end-point",
			"http://my.website.com:8080/end-point",
			"https://www.website.io:8080/endpoint",
			"https://my.website.dot.com:55555/end-point",
			"http://www.website.com:8080  ",
			"http://localhost.io:55555/endpoint",
			"http://my-website.com:8/new-end-point"
	})
	public void whenInputtedValidEngineAddress__thenNotThrowException(
			String engineAddress) {
		assertDoesNotThrow(() -> PROPERTIES_HANDLER.setBpmnEngineAddress(engineAddress));
	}

	@AfterClass
	public static void setEngineAddressDefaultValue() {
		PROPERTIES_HANDLER.setBpmnEngineAddress(INITIAL_ENGINE_ADDRESS);
	}

}