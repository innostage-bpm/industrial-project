package unit;

import innostage.camunda.utils.PropertiesHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ApplicationPropertiesTest {

    @Test
    public void applicationPropertiesFileShouldHaveEndpoints() {
        PropertiesHandler propertiesHandler = new PropertiesHandler();
        Assert.assertNotNull(propertiesHandler);
        Assert.assertNotNull(propertiesHandler.getProperty("process_definitions"));
        Assert.assertNotNull(propertiesHandler.getProperty("process_instances"));
        Assert.assertNotNull(propertiesHandler.getProperty("tasks"));
        Assert.assertNotNull(propertiesHandler.getProperty("executions"));
        Assert.assertNotNull(propertiesHandler.getProperty("users"));
        Assert.assertNotNull(propertiesHandler.getProperty("variable_instances"));
        Assert.assertNotNull(propertiesHandler.getProperty("variable_details"));
        Assert.assertNotNull(propertiesHandler.getProperty("incidents"));
    }

}
