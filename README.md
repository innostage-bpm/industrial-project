# InnoStage Industrial Project

This project was proposed by InnoStage company (emerged from ICL) as a part of MSIT-SE program in Innopolis University.

The short demo with first version of prototype [is here](https://youtu.be/AqRBtKwFfWY). 

Stage front: [http://bam-prototype.eastus.cloudapp.azure.com](http://bam-prototype.eastus.cloudapp.azure.com)

Stage back: [http://bam-prototype.eastus.cloudapp.azure.com:8081](http://bam-prototype.eastus.cloudapp.azure.com:8081)

Camunda engine: [http://bam-prototype.eastus.cloudapp.azure.com:8080/camunda](http://bam-prototype.eastus.cloudapp.azure.com:8080/camunda)

SonarQube: [http://bam-prototype.eastus.cloudapp.azure.com:9000](http://bam-prototype.eastus.cloudapp.azure.com:9000)

Production front: [https://bam-frontend.herokuapp.com](https://bam-frontend.herokuapp.com)

Production back: [https://bam-backend.herokuapp.com/swagger-ui.html](https://bam-back-end-api.herokuapp.com/swagger-ui.html)

Quick visual overview of project and work currently done is in [this presentation](https://gitlab.com/innostage-bpm/industrial-project/-/blob/2429ac21a66190974a268455fdc48a6896f49dfa/doc/presentations/EoSP_summer.pdf).

## Context

Each complex business process can be formalized and represented in [BPMN](https://ru.wikipedia.org/wiki/BPMN) notation.
Creating formal model of process helps to automate and optimize it  (decrease time of execution / human resources).
There are several commercial solutions, that are quite expensive and inflexible (IBM MQ, Oracle BAM)
There are plenty of open-source frameworks and engines, that are able to handle .bpmn files ([Activity](https://www.activiti.org/), [Flowable](https://flowable.com/open-source/), [Camunda](https://camunda.com/)).

[Camunda BPM](https://camunda.com/) (Workflow and Decision Automation Platform) was chosen in current project as a main framework. 
This decision was made by customer and can be considered as technical contraint.
Customer is developing own platform for business process automation and current project is tend to be only a part of huge system.

## Goal

The main goal of project is to provide Business Activity Monitoring (BAM) features such as process statistics, KPI control, alerts and notifications through the data presentation web-dashboard.

## Prototype

The prototype of business monitoring module is currently under development. 

The system is divided into [backend (Java Spring Boot, Hibernate, Postgres)](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/prototype-bam-system-backend) and [frontend part (React)](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/prototype-bam-system-frontend).

## Process

Roughly [project timeline](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/process%20management/Planning_TimeLine.pdf) can be divided into two parts:
1.  Requirements elicitation, research and experiments (feb-may)
2.  Development and implementation (june-aug)

Scrum for R&D with 2-week sprints (12h/week) was chosen as main process framework for the first semester.
From the second semester sprints are started 1-week long, because of more time allocated (24h/week) and more developemnt tasks assigned.

From the second semester all process management was migrated to [Jira + Confluence environemnt](https://innostage-industrial-project.atlassian.net)
All the details about process management in Gitlab that are described below were applicaple for the seven sprints in the first semester.

### Tasks in Gitlab

Each new task = new gitlab issue.

Task (issue) can be assigned to one or several persons ("assignee" field)
For each task should be a deadline ("due date" field)
If task was assigned to anyone and it became "ToDo", it should be assigned to exact sprint ("milestone" field)
There are two types of issues: "academic" (documenting) and "industrial" (coding), that can be shown by assigning lables.
If task is a specific system feature that should be implemented (not an ordinary R&D activity), it can be estimated in terms of story points by planning poker ("weight" field).
Each issue can be commented by all teammembers. Issue can be closed by creating merge request in the branch with the same name as issue (example: for issue "Make BPMN process compilable in Java project" should be created a branch with name "4-make-bpmn-process-of-mfc-compilable-in-java-project")

Backlog with list of all tasks is [here](https://gitlab.com/innostage-bpm/industrial-project/-/issues?scope=all&utf8=%E2%9C%93&state=all).

### [Milestones](https://gitlab.com/innostage-bpm/industrial-project/-/milestones)

Each new sprint = new gitlab milestone.
The list of milestones is [here](https://gitlab.com/innostage-bpm/industrial-project/-/milestones) with detailed information about its duration, tasks opened/closed and merge requests.
In each milestone there is information about total estimation time, time spent and more detailed info about issues with burndown chart ([example - sprint #3](https://gitlab.com/innostage-bpm/industrial-project/-/milestones/3))

### [Task workflow](https://gitlab.com/innostage-bpm/industrial-project/-/boards/1554438) 

Another visualization of backlog in Kanban board - [here](https://gitlab.com/innostage-bpm/industrial-project/-/boards/1554438).
In can be customized in different ways, but the main labels are:

*  Open - task was created/formulated, but it was not assigned to anyone and does not need to be done in current sprint. 
*  To Do - task that was assigned to person and need to be done in current sprint.
*  Doing - task has this status if person, who was assigned to this task started it.
*  Done - task was finished by assigned person, merge request was created, but not merged/not checked/not approved yet.
*  Closed - task was closed by approved merge request or was cancelled even before starting doing because of uselessness.

When task (issue) changes its status, it can be drag and dropped to another coloumn of board. 
Status can be changed manually by changing issue label in backlog.

### Time

Total time allocated to project (for **both** semesters) ~ 1400h

Each task after assigning to person should be estimated by tagging /estimate.
Each task after closing (finishing) should be commented by tagging /spend.

For collecting time spent by each sprint in speing semester external tool [gitpab](https://github.com/zubroide/gitpab) is used locally, it generates time reports based on moment when /timespent tag was created by each teammember.
In the summer semester we used Jira time reporting.

### Documentation Artifacts

All academic artifacts are located in folder ["doc"](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/doc):

In particular, here a short list of main docs about process management:

* [overall timeline](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/process%20management/Process_Planning_timeline.jpg)
* [requirements document](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/Requiments%20Specification.pdf)
* [architecture document](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/architecture/BAM_Architecture_final_report.pdf)
* [configuration management](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/process%20management/Configuration_Management.pdf)
* [team contract](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/process%20management/Team%20contract.pdf)
* [risk management](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/process%20management/Risk_Management_plan.pdf)
* [quality management](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/quality%20plan/QP-Milestone_4.pdf)
* [devops and CI/CD](https://gitlab.com/innostage-bpm/industrial-project/-/tree/stage/doc/devops)
* [testing report](https://gitlab.com/innostage-bpm/industrial-project/-/blob/9b877ae1b5137f5d1d3eb4424fa2fe2179f06d80/doc/quality%20plan/Testing_-_Final_Report.pdf)

Other interesting docs:

* [meeting protocols](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/doc%2Fmeetings) (naming: IU - in university, IS - in innostage office)
* [presentations of different stages](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/doc%2Fpresentations)
* [architecture design](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/doc%2Farchitecture)
* [quality plan](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/doc%2Fquality%20plan)

### Technical work

Results of experiments in first semester are here:

* [modelling BPMN diagram for MFC process](https://gitlab.com/innostage-bpm/industrial-project/-/blob/master/doc/bpmn%20diagrams/mfc-property-registration-process.png)
* [simulation of Camunda BMP incidents](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/experiments/process-incidents-simulation-war)
* [showing Camunda BPM metrics in Spring Boot app](https://gitlab.com/innostage-bpm/industrial-project/-/tree/master/experiments/spring-boot-web-app-metrics-collection)
* [collecting Camunda BPM metrics by Java API (MoSP Demo)](https://gitlab.com/innostage-bpm/industrial-project/-/tree/experiments/master/bpmn-process-example-metrics-war)
* [collecting Camunda BPM metrics by Web API on JS (EoSP Demo)](https://gitlab.com/innostage-bpm/industrial-project/-/tree/experiments/master/bam-metrics-monitor-js-demo)

